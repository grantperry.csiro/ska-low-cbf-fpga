# CAR_OCI_REGISTRY_HOST and PROJECT are combined to define
# the Docker tag for this project. The definition below inherits the standard
# value for CAR_OCI_REGISTRY_HOST = artefact.skao.int and overwrites
# PROJECT to give a final Docker tag
#
PROJECT = ska-low-cbf-fpga

# Fixed variables
# Timeout for gitlab-runner when run locally
TIMEOUT = 86400

CI_PROJECT_DIR ?= .


XAUTHORITY ?= $(HOME)/.Xauthority
THIS_HOST := $(shell ip a 2> /dev/null | sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p' | head -n1)
DISPLAY ?= $(THIS_HOST):0
JIVE ?= false# Enable jive
WEBJIVE ?= false# Enable Webjive

CI_PROJECT_PATH_SLUG ?= ska-low-cbf-fpga
CI_ENVIRONMENT_SLUG ?= ska-low-cbf-fpga
$(shell echo 'global:\n  annotations:\n    app.gitlab.com/app: $(CI_PROJECT_PATH_SLUG)\n    app.gitlab.com/env: $(CI_ENVIRONMENT_SLUG)' > gilab_values.yaml)

# define private overrides for above variables in here
-include PrivateRules.mak

#
# include makefile to pick up the standard Make targets, e.g., 'make build'
# build, 'make push' docker push procedure, etc. The other Make targets
# ('make interactive', 'make test', etc.) are defined in this file.
#
include .make/release.mk
#include .make/k8s.mk
include .make/make.mk
include .make/python.mk
#include .make/helm.mk
#include .make/oci.mk
include .make/help.mk
include .make/docs.mk

PYTHON_VARS_BEFORE_PYTEST = PYTHONPATH=src:src/ska_low_cbf_fpga

PYTHON_VARS_AFTER_PYTEST = -m "not post_deployment"

HELM_CHARTS_TO_PUBLISH ?= event-generator ska-low-cbf-fpga

PYTHON_BUILD_TYPE = non_tag_setup

requirements: ## Install Dependencies
	python3 -m pip install -r requirements-dev.txt


python-pre-lint: requirements## Overriding python.mk


python-pre-test: requirements## Overriding python.mk
	@mkdir -p build;

pipeline_unit_test: ## Run simulation mode unit tests in a docker container as in the gitlab pipeline
	@docker run --volume="$$(pwd):/home/tango/ska-low-cbf-fpga" \
		--env PYTHONPATH=src:src/ska_low_cbf_fpga --env FILE=$(FILE) -it $(ITANGO_DOCKER_IMAGE) \
		sh -c "cd /home/tango/ska-low-cbf-fpga && make requirements && make python-test"

.PHONY: all test help k8s lint logs describe namespace delete_namespace kubeconfig kubectl_dependencies k8s_test install-chart uninstall-chart reinstall-chart upgrade-chart interactive
