ARGS
----
ARGS (Automatic Register Generation System) is a tool to generate FPGA registers, producing both VHDL for FPGA synthesis
and other outputs to help build software to interface to the FPGA. One of these outputs is a Python file containing a
dictionary of all FPGA peripherals and their fields.

ICL
---
The ICL (Instrument Control Layer) is an abstraction layer for FPGA *Peripherals*. A peripheral here means a block of
functionality in the firmware.

The ICL is intended to provide a higher-level abstraction that combines register operations. For example, when
configuring the output packetiser, you might need to combine various flags into a bitfield, set source and destination
addresses, write a particular mode selection value into a control vector, and so on.

This detail is hidden within the ICL, and the control system developer can write something like:

.. code-block:: python

    icl_fpga.packetiser.configure(destination="10.1.1.1")
    icl_fpga.packetiser.on()

Design Principles
------------------
* Control system agnostic
* Facilitate code re-use between different FPGA personalities
    * E.g. A class per peripheral, just load the ones you need
* Consistent interface style between peripherals
* Sensible default values
