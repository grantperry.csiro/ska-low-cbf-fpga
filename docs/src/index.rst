.. toctree::
    :maxdepth: 2
    :caption: Contents:

.. toctree::
    :maxdepth: 2
    :caption: Context

    context

.. toctree::
    :maxdepth: 2
    :caption: User Guide

    user_guide

.. toctree::
    :maxdepth: 2
    :caption: Code Overview

    code_overview



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
