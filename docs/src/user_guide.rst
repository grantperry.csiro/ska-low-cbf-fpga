Quick-start
-----------
The bare minimum example: creating a driver, loading an ARGS map, and supplying them to an FpgaPersonality.

.. code-block:: python

    xcl = "/path/to/firmware.xclbin"
    args_cl = ArgsCl(xcl_file=xcl)
    args_map = ArgsMap.create_from_file(args_cl.get_map_build(), os.path.dirname(xcl))
    fpga = FpgaPersonality(args_cl, args_map)

    # read
    x = int(fpga.peripheral_name.field_name)
    z = fpga.another_peripheral.field.value

    # write
    fpga.peripheral_name.field_name = y

Creating a Personality
----------------------
The main thing is to provide the mapping from peripheral names (as reported in the map) to Python classes, using the
class variable `_peripheral_class`.

.. code-block:: python

    from ska_low_cbf_fpga import FpgaPersonality
    from packetiser import Packetiser

    class ExampleFpga(FpgaPersonality):
        _peripheral_class = {
            "packetiser": Packetiser,
        }

Advanced Personality Options
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
If multiple interfaces are used by an `FpgaPersonality`, they must be supplied to the constructor as a dictionary
mapping names (used by the _field_config variable inside `FpgaPeripheral`) to instances of `ArgsFpgaInterface` objects
which will be used. The default interface name can be specified (or it defaults to "__default__").

.. code-block:: python

    xcl = "/path/to/firmware.xclbin"
    args_cl = ArgsCl(xcl_file=xcl)  # direct access driver
    cl_poll = ArgsPolledAdapter(args_cl)  # periodic polling interface using args_cl
    interfaces = {
        "__default__": args_cl,
        "poll": cl_poll,
    }
    args_map = ArgsMap.create_from_file(args_cl.get_map_build(), os.path.dirname(xcl))
    fpga = FpgaPersonality(interfaces, args_map)

Creating a Peripheral
---------------------
* Derive your class from `FpgaPeripheral`.
* Write peripheral-specific methods & properties. You can use attribute style access to register values, or use the
  underlying peripheral dict-style access.   Return an `IclField` so the next layer up (control system mapping) has all
  the info it needs.

  * Use `user_write` and `user_error` fields in the object returned to provide configuration information to the
    control system.
* Specify methods & attributes that should be exposed to the control system in `_user_methods` and `_user_attributes`.
  Attributes are also marked as writable (or not) with a boolean value, but note this not enforced, it's just a
  suggestion to the control system layer.

* Special keys can be included in `_user_attributes` to control automatic discovery of user attributes. By default,
  discovery is performed only if `_user_attributes` is empty, and registers are only discovered if no properties are
  discovered.

    * `DISCOVER_PROPERTIES`: Always discover properties
    * `DISCOVER_REGISTERS`: Always discover registers
    * `DISCOVER_ALL`: Always discover both properties & registers

.. code-block:: python

        class ExamplePeripheral(FpgaPeripheral):
            # Hints for the control system
            _user_methods = {"on"}

            # Note: if not specified, _user_attributes will include all properties.
            # If no properties exist, it will include all fields.
            # Set _user_attributes = None if you do not want to expose any fields/properties.
            # Use the DISCOVER_ constants to discover more than the defaults.
            _user_attributes = {"reset_active", "something_wrong", "dst_ip"}

            # Simple read-only property example
            @property
            def reset_active(self) -> IclField[bool]:
                # attribute-style access (beware when overloading names)
                reset_active = self.module_reset == 1
                return IclField(description="Reset Active", type_=bool, value=reset_active, format="%s", user_write=False)

            # Error condition example
            @property
            def something_wrong(self) -> IclField[bool]:
                error = self.error_1 or self.error_2
                return IclField(description="something wrong", type_=bool, value=error, user_error=True, user_write=False)

            # Method example
            def on(self):
                # dict-style access
                self["control_vector"] = 3

            # Get/Set property example
            DATA_IP_DST = 8  # class variable for address within RAM buffer

            @property
            def dst_ip(self) -> IclField[str]:
                """Destination IPv4 address"""
                dst_ip = self.data[DATA_IP_DST]
                ip = socket.inet_ntoa(int(dst_ip).to_bytes(4, "big"))
                return IclField(description="Destination IPv4 Address", type_=str, value=ip, format="%s", user_write=True)

            @dst_ip.setter
            def dst_ip(self, ip: str):
                """Set destination IPv4 address"""
                self.data[DATA_IP_DST] = socket.inet_aton(ip)

Advanced Peripheral Options
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Special field configuration can be provided in `_field_config`, for example if you need to use different interfaces for
some fields, or if you wish to override the ARGS-provided description, specify a non-integer type, etc.

The keys in this dictionary must match the name of the field from the ARGS map.

.. code-block:: python

        class FieldsExample(FpgaPeripheral):
            _field_config = {
                "polled_example": IclFpgaField(read_interface="poll", write_interface="poll"),
                "description_example": IclFpgaField(description="my desc."),
                "error_example": IclFpgaField(user_error=True),
                "read_only_suggestion_to_control_system": IclFpgaField(user_write=False),
            }

Status Monitoring
-----------------

Only implemented via `pyxrt`.

`ArgsXrt` will create an instance of an `XrtInfo` object, which can be accessed through
an `FpgaPersonality` object. Most information is accessed via array item syntax,
with an exception of `xclbin_uuid` as a property. Some parameters return complex data
structures.

.. code-block:: python

    # get UUID of xclbin file loaded to card (str)
    fpga.info.xclbin_uuid

    # get card's serial number (str)
    fpga.info["platform"]["controller"]["card_mgmt_controller"]["serial_number"]

Note that the return types vary:

.. code-block:: python

    for item in fpga.info:
        print(item, type(fpga.info[item]))

.. code-block::

    bdf <class 'str'>
    dynamic_regions <class 'str'>
    electrical <class 'dict'>
    host <class 'dict'>
    interface_uuid <class 'str'>
    kdma <class 'bool'>
    m2m <class 'bool'>
    max_clock_frequency_mhz <class 'int'>
    mechanical <class 'dict'>
    memory <class 'dict'>
    name <class 'str'>
    nodma <class 'bool'>
    offline <class 'bool'>
    pcie_info <class 'dict'>
    platform <class 'dict'>
    thermal <class 'list'>
