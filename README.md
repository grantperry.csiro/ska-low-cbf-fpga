# Low.CBF FPGA Software Interface #

[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-low-cbf-fpga/badge/?version=latest)](https://developer.skao.int/projects/ska-low-cbf-fpga/en/latest/?badge=latest)

Control & monitoring interface to an Alveo FPGA card with an ARGS register interface.
This software is otherwise independent of control system or FPGA firmware image.

# Installation

This is published as a Python package, so end users can just run:

    pip3 install --extra-index-url https://artefact.skao.int/repository/pypi-internal/simple ska-low-cbf-fpga

Developers should use `poetry install` to create a virtual environment.
Developers who wish to use the PyOpenCL driver can use `poetry install -E cl`.

# Changelog

## 0.13.0
* Add IclFpgaBitfield object, for representing individual bits within an ARGS
register
## 0.12.0
* ArgsMap & ArgsSimulator constructors now take dict objects rather than file
paths. Class methods `.create_from_file` are provided to implement the old
behaviour.
### 0.11.1
* FpgaPeripheral:
  * Fix \_user\_attributes leakage bug. It was inadvertantly
modifying the instance inherited from FpgaUserInterface.
  * Remove discovery configuration values from user\_attributes before
returning it for external use.
## 0.11
* ArgsCl: restore operation of writing to registers
* FpgaPeripheral: add flags to control user\_attribute discovery:
DISCOVER\_REGISTERS, DISCOVER\_PROPERTIES, DISCOVER\_ALL
* XrtInfo: new class to report card health & other status information
### 0.10.2
* ArgsFpgaInterface: Add arguments for partial memory buffer reads
* ArgsXrt, ArgsCl:
  * Memory buffer read/write indices now match command-line configuration order
  * Read partial memory buffers (CL still transfers the full buffer but returns
* **Warning** ArgsCl is unable to write to registers in this version!
only the region of interest, XRT does a true partial read)
  * Speed improvements to ARGS read/write
* FpgaPersonality: Add shortcut to read_memory & write_memory functions of the
default interface
### 0.10.1
* fpga\_cmdline: Add IPython console, multiple FPGA card support, and optional
pyopencl installation
* ArgsXrt: Change handling of Compute Unit name (now takes only the part before
the colon)
## 0.10.0
* FpgaPeripheral: Change attribute writable discovery mechanism & error state
reporting.
  * Attribute discovery - the IclField object returned by each attribute now
contains a `user_write` field, to be used by the control system. This setting is
not checked/enforced at the ICL.
  * Error condition reporting - the returned IclField object now contains a
`user_error` configuration field. Previously an "errors" attribute was used (a
set of strings describing active errors).
### 0.9.1
* register\_load: Interpret offsets in text file as bytes (not words)
* IclField: Remove debug print
* str\_from\_int\_bytes: Report decimal values (were accidentally rounded)
## 0.9.0
* fpga\_cmdline: Add register text file loader, make interactive mode optional
* IclFpgaField: Fix operators (they were inadvertantly replaced by dataclass)
### 0.8.2
* ArgsSimulator: Fix bug with read/write multiple words
### 0.8.1
* FpgaPeripheral: Fix bug where \_field\_config defaults (None) were incorrectly
replacing sensible default values from ArgsMap.
## 0.8.0
* ArgsFpgaInterface: Add memory buffer read & write functions
(read\_memory, write\_memory)
* ArgsCl, ArgsXrt: Implement write\_memory
### 0.7.2
* Configure package to support Python v3.6+ (was 3.8+)
### 0.7.1
* Change package name to ska\_low\_cbf\_fpga (was ska-low-cbf-fpga)
## 0.7.0
* New repository
  * Splits generic FPGA interface away from software that is specific to a
control system (Tango device) or firmware image (PSR packetiser peripheral)
## 0.6.0
* ArgsXrt created, a new ArgsFpgaDriver that works via the Xilinx XRT Python
bindings, [pyxrt](https://xilinx.github.io/XRT/master/html/pyxrt.html)
### 0.5.4
* ArgsCl: Add support for read & write of arrays larger than exchange buffer
### 0.5.3
* IclField: Add operators and integer typecast
### 0.5.2
* Fix tests that broke in 0.5.1:
  * fpga\_icl.py: Rename imports so type checks work when testing
  * tests/conftest.py: Update patching of args\_map
  ("\_load\_map" function renamed to "load\_map")
### 0.5.1
* ArgsSimulator added, an FPGA simulation driver
* ArgsPolledAdapter: Make polling loop do reads in blocks
* IclFpgaField: Fix addressing of elements within value arrays
## 0.5.0
* Major refactor. AlveoCL split into smaller, more modular, pieces: ArgsMap, ArgsCl,
ArgsFpgaDriver etc.
