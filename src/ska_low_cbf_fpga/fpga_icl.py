# -*- coding: utf-8 -*-
#
# Copyright (c) 2021 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.

"""
Instrument Control Layer
"""
import dataclasses
import logging
import typing
from dataclasses import dataclass, field

import numpy as np

from ska_low_cbf_fpga.args_fpga import (
    WORD_SIZE,
    ArgsFpgaInterface,
    ArgsWordType,
)
from ska_low_cbf_fpga.args_map import ArgsFieldInfo, ArgsMap

T = typing.TypeVar("T")


@dataclass
class IclField(ArgsFieldInfo, typing.Generic[T]):
    """ICL field, probably derived from one or more IclFpgaFields"""

    # user configuration
    type_: type = field(default=T)
    format: str = field(default="%i")
    user_write: bool = True
    """
    Should the control system allow writing to this field?
    Note: not enforced at ICL!
    """
    user_error: bool = False
    """
    Should the control system treat a non-zero value as an error/alarm?
    """
    # dynamic
    value: T = field(default=None)

    # note: ARGS map fields inherited from ArgsFieldInfo

    def __int__(self):
        return int(self.value)

    def __bool__(self):
        return bool(self.value)

    # operators
    def __add__(self, other):
        if isinstance(other, IclField):
            return self.value + other.type_(other)
        return self.value + other

    def __radd__(self, other):
        return self.__add__(other)

    def __sub__(self, other):
        if isinstance(other, IclField):
            return self.value - other.type_(other)
        return self.value - other

    def __rsub__(self, other):
        if isinstance(other, IclField):
            return other.type_(other) - self.value
        return other - self.value

    def __mul__(self, other):
        if isinstance(other, IclField):
            return self.value * other.type_(other)
        return self.value * other

    def __rmul__(self, other):
        return self.__mul__(other)

    def __pow__(self, other):
        if isinstance(other, IclField):
            return self.value ** other.type_(other)
        return self.value ** other

    def __rpow__(self, other):
        if isinstance(other, IclField):
            return other.type_(other) ** self.value
        return other ** self.value

    def __truediv__(self, other):
        if isinstance(other, IclField):
            return self.value / other.type_(other)
        return self.value / other

    def __rtruediv__(self, other):
        if isinstance(other, IclField):
            return other.type_(other) / self.value
        return other / self.value

    def __floordiv__(self, other):
        if isinstance(other, IclField):
            return self.value // other.type_(other)
        return self.value // other

    def __rfloordiv__(self, other):
        if isinstance(other, IclField):
            return other.type_(other) // self.value
        return other // self.value

    def __mod__(self, other):
        if isinstance(other, IclField):
            return self.value % other.type_(other)
        return self.value % other

    def __rmod__(self, other):
        if isinstance(other, IclField):
            return other.type_(other) % self.value
        return other % self.value

    def __lshift__(self, other):
        if isinstance(other, IclField):
            return self.value << other.type_(other)
        return self.value << other

    def __rlshift__(self, other):
        if isinstance(other, IclField):
            return other.type_(other) << self.value
        return other << self.value

    def __rshift__(self, other):
        if isinstance(other, IclField):
            return self.value >> other.type_(other)
        return self.value >> other

    def __rrshift__(self, other):
        if isinstance(other, IclField):
            return other.type_(other) >> self.value
        return other >> self.value

    def __and__(self, other):
        if isinstance(other, IclField):
            return self.value & other.type_(other)
        return self.value & other

    def __rand__(self, other):
        return self.__and__(other)

    def __or__(self, other):
        if isinstance(other, IclField):
            return self.value | other.type_(other)
        return self.value | other

    def __ror__(self, other):
        return self.__or__(other)

    def __xor__(self, other):
        if isinstance(other, IclField):
            return self.value ^ other.type_(other)
        return self.value ^ other

    def __rxor__(self, other):
        return self.__xor__(other)

    def __invert__(self):
        return ~self.value

    # comparisons
    def __lt__(self, other):
        if isinstance(other, IclField):
            return self.value < other.type_(other)
        return self.value < other

    def __le__(self, other):
        if isinstance(other, IclField):
            return self.value <= other.type_(other)
        return self.value <= other

    def __eq__(self, other):
        if isinstance(other, IclField):
            return self.value == other.type_(other)
        return self.value == other

    def __ne__(self, other):
        if isinstance(other, IclField):
            return self.value != other.type_(other)
        return self.value != other

    def __gt__(self, other):
        if isinstance(other, IclField):
            return self.value > other.type_(other)
        return self.value > other

    def __ge__(self, other):
        if isinstance(other, IclField):
            return self.value >= other.type_(other)
        return self.value >= other


# The eq and order parameters are False to stop the dataclass decorator from
# overriding the operators defined in IclField
@dataclass(eq=False, order=False)
class IclFpgaField(IclField[ArgsWordType]):
    """An ICL field that is linked to an FpgaInterface"""

    # configuration
    type_: type = ArgsWordType
    read_interface: typing.Union[str, ArgsFpgaInterface] = None
    write_interface: typing.Union[str, ArgsFpgaInterface] = None

    # dynamic
    value: ArgsWordType = field(init=False)

    @property
    def value(self) -> typing.Union[int, np.ndarray]:
        """Read current value from FPGA interface"""
        return self.read_interface.read(self.address, self.length)

    @value.setter
    def value(self, value: typing.Union[int, np.ndarray]):
        """Write new value via FPGA interface"""
        # on initialisation, the property object is passed to the setter
        # we don't want to send this to the FPGA
        # (an alternative could be to set an "init_complete" bool in __post_init__)
        if type(value) is not property:
            self.write_interface.write(self.address, value)

    def __getitem__(self, item: int) -> ArgsWordType:
        """Array access for multi-word values"""
        if item >= self.length:
            raise IndexError("item index out of range")
        if item < 0:
            raise NotImplementedError("negative indexes are not implemented")
        return self.read_interface.read(self.address + item * WORD_SIZE, 1)

    def __setitem__(self, key: int, value: ArgsWordType):
        """Array access for multi-word values"""
        if key >= self.length:
            raise IndexError("item index out of range")
        if key < 0:
            raise NotImplementedError("negative indexes are not implemented")
        self.write_interface.write(self.address + key * WORD_SIZE, value)


# The eq and order parameters are False to stop the dataclass decorator from
# overriding the operators defined in IclField
@dataclass(eq=False, order=False)
class IclFpgaBitfield(IclFpgaField):
    """An ICL field representing a single bit in an underlying FPGA register"""

    _mask: int = None  # e.g. 0b00001000
    type_: type = bool

    def __post_init__(self):
        self._mask = 1 << self.bit_offset

    @property
    def value(self) -> typing.Union[int, np.ndarray]:
        """Read current value from FPGA interface"""
        return bool(
            self.read_interface.read(self.address, self.length) & self._mask
        )

    @value.setter
    def value(self, value: bool):
        """Write new value via FPGA interface"""
        # on initialisation, the property object is passed to the setter
        # we don't want to send this to the FPGA
        # (an alternative could be to set an "init_complete" bool in __post_init__)
        if type(value) is not property:
            # FIXME: ideally we would lock the driver for this...
            #  but the driver controls the lock internally so that's a job for later
            inital_value = self.read_interface.read(self.address, self.length)
            if value:
                # turn our bit on
                new_value = inital_value | self._mask
            else:
                # turn our bit off
                new_value = inital_value & (~self._mask)
            self.write_interface.write(self.address, new_value)

    # TODO - is there a better way to un-define __getitem__ and __setitem__ ?
    def __getitem__(self, item):
        raise NotImplementedError

    def __setitem__(self, key, value):
        raise NotImplementedError


class FpgaUserInterface:
    """
    A common interface used by FpgaPeripheral and FpgaPersonality for exposing
    attributes and methods to the control system.
    """

    _user_attributes = set()
    """
    Attributes to be exposed to the control system.
    Defaults to properties (if configured), or FPGA registers.
    Set to None if you really want no attributes exposed.
    """

    _user_methods = set()
    """Methods to be exposed to the control system."""

    def __init__(self):
        # we do not want object instances to modify the base class values!
        if self._user_attributes is FpgaUserInterface._user_attributes:
            self._user_attributes = set(self._user_attributes)
        if self._user_methods is FpgaUserInterface._user_methods:
            self._user_methods = set(self._user_methods)

    @property
    def user_attributes(self) -> typing.Set[str]:
        """
        Attributes to be exposed to the control system.
        Defaults to all registers (read only) if _user_attributes is empty.
        """
        if self._user_attributes is not None:
            return self._user_attributes
        return set()

    @property
    def user_methods(self) -> typing.Set[str]:
        """Methods to be exposed to the control system."""
        return self._user_methods


def _interfaces_enforce_dict(interfaces) -> {str: ArgsFpgaInterface}:
    if isinstance(interfaces, ArgsFpgaInterface):
        return {"__default__": interfaces}
    elif (
        isinstance(interfaces, dict)
        and set(type(_) for _ in interfaces.keys()) == {str}
        and all(
            [isinstance(_, ArgsFpgaInterface) for _ in interfaces.values()]
        )
    ):
        return interfaces
    else:
        raise TypeError(
            f"interfaces parameter is of type {type(interfaces)}, but should be an "
            f"ArgsFpgaInterface or dictionary of str:ArgsFpgaInterface"
        )


DISCOVER_REGISTERS = "__discover_registers__"
DISCOVER_PROPERTIES = "__discover_properties__"
DISCOVER_ALL = "__discover_all__"
MAGIC_USER_ATTRIBUTES = [DISCOVER_ALL, DISCOVER_PROPERTIES, DISCOVER_REGISTERS]


class FpgaPeripheral(FpgaUserInterface):
    """Base class that provides functions common to all FPGA Peripherals."""

    _field_config = {}
    """
    Configuration values for fields read from FPGA.
    e.g. to use an interface called 'poll' for the field called 'example':
    _field_config = {"example": IclFpgaField(read_interface="poll")}
    """

    def __init__(
        self,
        interfaces: typing.Union[
            ArgsFpgaInterface, typing.Dict[str, ArgsFpgaInterface]
        ],
        map_field_info: typing.Dict[str, ArgsFieldInfo],
        default_interface: str = "__default__",
    ):
        """
        FpgaPeripheral constructor.
        Creates an instance of IclFpgaField for each field defined in the map.
        Populates _user_attributes if not set by derived class.
        :param interfaces: One or more ArgsFpgaInterfaces. If more than one, supply as
        dict mapping names (str) to instances.
        :param map_field_info: Most likely a portion of an ArgsMap.
        """
        super().__init__()
        self._fields = {}
        self._default_interface = default_interface
        # if only an interface object was supplied (rather than a dict),
        # set it up as our default interface
        self._interfaces = _interfaces_enforce_dict(interfaces)

        # configure fields
        for name, info in map_field_info.items():
            # start with info from map
            field_params = dataclasses.asdict(info)
            if name in self._field_config:
                # allow user config to replace info from map
                # first filter out None values so they don't overwrite defaults
                user_config = {
                    k: v
                    for k, v in dataclasses.asdict(
                        self._field_config[name]
                    ).items()
                    if v is not None
                }
                field_params.update(user_config)
            # link to read/write interface objects
            for interface in ["read_interface", "write_interface"]:
                if interface in field_params:
                    # if user specifies a name, look up the matching object
                    requested_interface = field_params[interface]
                else:
                    requested_interface = self._default_interface
                field_params[interface] = self._interfaces[requested_interface]
            if info.bit_offset is not None:
                # bitfield
                self._fields[name] = IclFpgaBitfield(**field_params)
            else:
                # regular field
                self._fields[name] = IclFpgaField(**field_params)

        # setup user_attributes - don't expose any attributes if value is "None"
        if self._user_attributes is not None:
            assert type(self._user_attributes) == set
            # if user attributes is empty, or the user requested it,
            # expose all properties
            if (
                not self._user_attributes
                or DISCOVER_PROPERTIES in self._user_attributes
                or DISCOVER_ALL in self._user_attributes
            ):
                # find properties, set writable to True if they have a setter configured
                properties = [
                    _
                    for _ in dir(self)
                    if isinstance(getattr(self.__class__, _, None), property)
                ]
                # remove 'internal use only' properties
                properties.remove("user_attributes")
                properties.remove("user_methods")
                self._user_attributes |= set(properties)

            # if user attributes is still empty, or the user requested it,
            # add all registers as read-only
            if (
                not self._user_attributes
                or DISCOVER_REGISTERS in self._user_attributes
                or DISCOVER_ALL in self._user_attributes
            ):
                self._user_attributes |= set(self._fields.keys())

            # remove any special configuration flags
            for magic_value in MAGIC_USER_ATTRIBUTES:
                if magic_value in self._user_attributes:
                    self._user_attributes.remove(magic_value)

        self._init_complete = True

    def __dir__(self) -> list:
        """Add our list of registers to the default directory"""
        return list(super().__dir__()) + list(self._fields.keys())

    def __getattr__(self, name: str) -> IclFpgaField:
        """Attribute access to fields"""
        # TODO - is there a way to inspect the AttributeError that resulted in this
        #  function being called?  It can mask some errors!!
        if name in self._fields.keys():
            return self._fields[name]
        else:
            if name in self.__dir__():
                raise AttributeError(
                    f"AttributeError occurred reading "
                    f"{self.__class__.__name__}.{name}. Good luck!"
                )
            else:
                raise AttributeError(
                    f"{self.__class__.__name__} has no attribute {name}"
                )

    def __setattr__(self, key: str, value):
        """Pass on attribute writes to the field, where appropriate"""
        if "_init_complete" in self.__dict__ and key in self._fields:
            self._fields[key].value = value
        else:
            super().__setattr__(key, value)

    def __getitem__(self, item: str) -> IclFpgaField:
        """Index style access to fields"""
        return self._fields[item]

    def __setitem__(self, key: str, value):
        """Pass on writes to the field"""
        self._fields[key].value = value


class FpgaPersonality(FpgaUserInterface):
    """
    Creates peripheral objects for each peripheral of an AlveoCL FPGA object.
    """

    _peripheral_class = {}
    """
    Dict that maps from peripheral name (str) to class to be used.
    If a peripheral is not listed in peripheral_class, a default FpgaPeripheral
    object will be created.
    e.g.
    _peripheral_class = {"packetiser": Packetiser}
    """

    def __init__(
        self,
        interfaces: typing.Union[
            ArgsFpgaInterface, typing.Dict[str, ArgsFpgaInterface]
        ],
        map_: ArgsMap,
        logger: logging.Logger = None,
    ):
        """
        :param interfaces: One or more ArgsFpgaInterfaces. If more than one, supply as
        dict mapping names (str) to instances.
        :param map_: ARGS peripheral/register map.
        """
        super().__init__()
        if logger is None:
            self._logger = logging.getLogger()
        else:
            self._logger = logger
        self._logger.debug("FPGA Personality init")
        self._peripherals = {}

        # provide shortcut access to useful things
        self.default_interface = _interfaces_enforce_dict(interfaces)[
            "__default__"
        ]
        self.read_memory = self.default_interface.read_memory
        self.write_memory = self.default_interface.write_memory
        self.info = getattr(self.default_interface, "info", None)

        # create Peripherals
        for peripheral_name in map_.keys():
            if peripheral_name in self._peripherals:
                raise NotImplementedError(
                    "Can't do multiple instances of a peripheral"
                )

            try:
                class_ = self._peripheral_class[peripheral_name]
            except KeyError:
                # default to generic class
                class_ = FpgaPeripheral
            peripheral = class_(interfaces, map_[peripheral_name])

            self._logger.info(
                f"Created peripheral {peripheral_name} using class {class_.__name__}"
            )
            self._peripherals[peripheral_name] = peripheral

    def __getattr__(self, name: str):
        """Access peripherals by attribute syntax"""
        if name in self._peripherals:
            return self._peripherals[name]
        raise AttributeError(f"No peripheral '{name}'")

    def __dir__(self):
        """Add our list of peripherals to the default directory"""
        return list(super().__dir__()) + self.peripherals

    def __getitem__(self, item: str) -> FpgaPeripheral:
        """Index style access to peripheral objects"""
        return self._peripherals[item]

    @property
    def peripherals(self) -> typing.List[str]:
        """
        :return: List of peripheral names
        """
        return list(self._peripherals.keys())
