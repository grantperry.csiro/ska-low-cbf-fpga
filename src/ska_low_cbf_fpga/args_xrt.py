import threading
import typing

import numpy as np
import pyxrt

from ska_low_cbf_fpga.args_fpga import (
    ARGS_SHARED_ADDRESS,
    EXCHANGE_BUF_CONFIG,
    WORD_SIZE,
    ArgsFpgaDriver,
    ArgsWordType,
    MemConfig,
    mem_parse,
    str_from_int_bytes,
)
from ska_low_cbf_fpga.xrt_info import XrtInfo

"""
FPGA Driver using Xilinx's XRT Python bindings (pyxrt).
You will need to have the pyxrt module available!
"""


class ArgsXrt(ArgsFpgaDriver):
    """
    pyxrt-based FPGA Driver.
    Requires a .xclbin file.
    """

    def _setup(
        self,
        xcl_file: str,  # TODO use a file object instead?
        mem_config: typing.Union[list, str] = "",
        card: int = 0,
    ):
        """
        :param xcl_file: path to a .xclbin FPGA kernel
        :param mem_config: a list of MemConfig tuples (<size in bytes>, <shared?>)
            the first list item is used to send/receive register values
        :param card: int device index
        """

        self._card = card
        self._mem_config = mem_config
        self._buf = []  # buffer objects
        self._shared_bufs = []
        """List of buffer indexes that are shared between host & FPGA"""
        self._device = None
        self._run = None
        self._lock = (
            threading.Lock()
        )  # to prevent multiple simultaneous kernel calls
        self._xcl_file = xcl_file
        self._mem_group_ids = []
        self._device = pyxrt.device(self._card)
        self.logger.debug("device: {}".format(self._device))
        self.info = XrtInfo(self._device)

    def _load_firmware(self):
        """
        Load the binary to the FPGA (if required) and initialise our kernel object
        """
        self.logger.debug("Loading {}".format(self._xcl_file))
        xclbin = pyxrt.xclbin(self._xcl_file)
        uuid = self._device.load_xclbin(xclbin)

        self.logger.debug("xclbin has these IPs:")
        ip_names = []
        for ip in xclbin.get_ips():
            ip_names.append(ip.get_name())
            self.logger.debug(ip.get_name())

        # Note: cu_name used to be just ip_names[0], no split.
        # Older version of XRT accepts both forms, new version only accepts
        # the split...
        cu_name = ip_names[0].split(":")[0]
        self.logger.debug(f"Using Compute Unit name {cu_name}")
        kernel = pyxrt.kernel(
            self._device, uuid, cu_name, pyxrt.kernel.cu_access_mode(0)
        )

        num_args = 0
        for n in range(100):
            try:
                gid = kernel.group_id(n)
                num_args += 1
                if gid != -1:
                    self._mem_group_ids.append(gid)
                    self.logger.debug(
                        f"Kernel group: {n}, memory bank {kernel.group_id(n)}"
                    )
            except IndexError:
                break

        self.logger.debug("kernel has {} args:".format(num_args))

        self._init_buffers()
        self._run = kernel(
            0,  # source address
            ARGS_SHARED_ADDRESS,  # destination address
            self._buf[0],
            64,  # length
            *self._buf[1:],
        )

    def _init_buffers(self):
        """
        Create XRT & Numpy buffers
        """
        mem_config = self._mem_config
        if (
            mem_config
            and isinstance(mem_config, list)
            and all(isinstance(_, MemConfig) for _ in mem_config)
        ):
            self._mem_config = mem_config
        elif isinstance(mem_config, str):
            self._mem_config = mem_parse(mem_config)
        else:
            self.logger.warning(
                "you should supply a list of MemConfigs - using default"
                "interchange buffer {}".format(EXCHANGE_BUF_CONFIG)
            )
            self._mem_config = [EXCHANGE_BUF_CONFIG]
        flags = pyxrt.bo.flags
        mem_group_ids = self._mem_group_ids.copy()
        for index, memory in enumerate(self._mem_config):
            mem_group_id = mem_group_ids.pop(0)
            if memory.shared:
                self.logger.debug(
                    "allocating {} of shared memory".format(
                        str_from_int_bytes(memory.size)
                    )
                )
                if memory.size % WORD_SIZE:
                    raise ValueError(
                        "memory size not a multiple of {} bytes".format(
                            WORD_SIZE
                        )
                    )
                self._buf.append(
                    pyxrt.bo(
                        self._device, memory.size, flags.normal, mem_group_id
                    )
                )
                self._shared_bufs.append(index)

            else:
                self.logger.debug(
                    "allocating {} of device-only memory".format(
                        str_from_int_bytes(memory.size)
                    )
                )
                self._buf.append(
                    pyxrt.bo(
                        self._device,
                        memory.size,
                        flags.device_only,
                        mem_group_id,
                    )
                )
        self.EXCHANGE_BUF_BYTES = self._buf[0].size()
        self.EXCHANGE_BUF_WORDS = self.EXCHANGE_BUF_BYTES // WORD_SIZE

    def read(self, source: int, length: int = 1):
        """
        Read values from FPGA

        :param source: Byte address to start reading from
        :param length: Number of words to read
        :return: value(s) from FPGA
        """
        with self._lock:
            self._run.set_arg(1, ARGS_SHARED_ADDRESS)
            total_bytes = length * WORD_SIZE
            values = np.empty(total_bytes, dtype=np.uint8)
            for byte_offset in range(0, total_bytes, self.EXCHANGE_BUF_BYTES):
                read_address = source + byte_offset
                bytes_remaining = total_bytes - byte_offset
                valid_bytes = min(bytes_remaining, self.EXCHANGE_BUF_BYTES)
                self._read_args_page(read_address, valid_bytes)
                values[byte_offset : byte_offset + valid_bytes] = self._buf[
                    0
                ].read(valid_bytes, 0)
            if length == 1:
                return int(values.view(dtype=ArgsWordType))
            return values.view(dtype=ArgsWordType)

    def _read_args_page(self, address_start: int, n_bytes: int = None):
        """
        Read one block of register values into host memory buffer
        :param address_start: Byte address to start reading from
        :param n_bytes: Number of bytes to read
        """
        if n_bytes is None:
            n_bytes = self.EXCHANGE_BUF_BYTES
        else:
            n_bytes = min(n_bytes, self.EXCHANGE_BUF_BYTES)
        if self._lock.locked():
            # set kernel arguments & execute kernel
            self._run.set_arg(0, address_start)
            self._run.set_arg(3, n_bytes)
            if self._run.state() != pyxrt.ert_cmd_state.ERT_CMD_STATE_NEW:
                self._run.start()
            self._run.wait(1)  # timeout in ms
            # migrate to host
            self._buf[0].sync(
                pyxrt.xclBOSyncDirection.XCL_BO_SYNC_BO_FROM_DEVICE,
                n_bytes,
                0,
            )
        else:
            raise RuntimeError("_read_args_page called without lock")

    def write(self, destination: int, values: typing.Union[int, np.ndarray]):
        """
        Write values to FPGA

        :param destination: FPGA byte address where writes should start
        :param values: value(s) to write, if more than one value they must be words,
         to be written to consecutive words (i.e. byte addresses increment by WORD_SIZE)
        """
        if isinstance(values, (int, np.integer)):
            values = np.array(values, dtype=ArgsWordType, ndmin=1)
        assert values.dtype == ArgsWordType

        with self._lock:
            self._run.set_arg(0, ARGS_SHARED_ADDRESS)
            for word_offset in range(0, values.size, self.EXCHANGE_BUF_WORDS):
                write_address = destination + word_offset * WORD_SIZE
                words_remaining = values.size - word_offset
                # number of words that are valid to write this loop iteration
                valid_words = min(words_remaining, self.EXCHANGE_BUF_WORDS)
                self._write_args_page(
                    write_address,
                    values[word_offset : word_offset + valid_words],
                )

    def _write_args_page(self, destination, values):
        """
        Write one block of register values from host buffer to FPGA
        :param destination: Byte address to start writing to
        :param values: Array of values to be written to consecutive words
        """
        if self._lock.locked():
            # copy input data into shared buffer
            self._buf[0].write(values, 0)
            # migrate from host
            self._buf[0].sync(
                pyxrt.xclBOSyncDirection.XCL_BO_SYNC_BO_TO_DEVICE,
                values.nbytes,
                0,
            )
            # set kernel arguments & execute kernel
            self._run.set_arg(1, destination)
            self._run.set_arg(3, values.nbytes)
            if self._run.state() != pyxrt.ert_cmd_state.ERT_CMD_STATE_NEW:
                self._run.start()
            self._run.wait(1)  # timeout in ms
        else:
            raise RuntimeError("_write_args_page called without lock")

    def read_memory(
        self, index: int, size_bytes: int = None, offset_bytes: int = 0
    ) -> np.ndarray:
        """
        Read a shared memory buffer.
        :param size_bytes: number of bytes to transfer
        (transfers the whole buffer if not specified or None)
        :param offset_bytes: starting address
        :param index: Index of the shared buffer to save.
        Zero is the ARGS interchange buffer, which you probably don't want.
        :return: shared memory buffer
        """
        assert (
            index in self._shared_bufs
        ), f"Buffer #{index} is not a shared memory"
        if size_bytes is None:
            size_bytes = self._buf[index].size()
        with self._lock:
            self._buf[index].sync(
                pyxrt.xclBOSyncDirection.XCL_BO_SYNC_BO_FROM_DEVICE,
                size_bytes,
                offset_bytes,
            )
        return (
            self._buf[index].read(size_bytes, offset_bytes).view(ArgsWordType)
        )

    def write_memory(
        self, index: int, values: np.ndarray, offset_bytes: int = 0
    ):
        """
        Write to a shared memory buffer.
        :param index: Index of the shared buffer to write to.
        Zero is the ARGS interchange buffer, which you probably don't want.
        :param values: Data to write.
        :param offset_bytes: Byte-based offset where values should start in
        buffer.
        """
        assert (
            index in self._shared_bufs
        ), f"Buffer #{index} is not a shared memory"
        with self._lock:
            self._buf[index].write(values.view(dtype=np.uint8), offset_bytes)
            self._buf[index].sync(
                pyxrt.xclBOSyncDirection.XCL_BO_SYNC_BO_TO_DEVICE,
                values.nbytes,
                offset_bytes,
            )
