import os.path
import re

import numpy as np

from .args_fpga import (
    EXCHANGE_BUF_CONFIG,
    WORD_SIZE,
    ArgsFpgaDriver,
    ArgsWordType,
)
from .args_map import load_map

BLOCK_LENGTH = EXCHANGE_BUF_CONFIG.size // WORD_SIZE

"""
Simulate an ARGS-based FPGA, for development/testing.
"""


class ArgsSimulator(ArgsFpgaDriver):
    """
    FPGA Simulation driver.
    """

    @classmethod
    def create_from_file(cls, fpgamap_path: str):
        """
        :param fpgamap_path: Path to fpgamap_<numbers>.py file to be simulated.
        """
        directory = os.path.dirname(fpgamap_path)
        filename = os.path.basename(fpgamap_path)
        pattern = re.compile("fpgamap_([\\d]+)\\.py")
        try:
            build = int(pattern.match(filename)[1], 16)
        except (IndexError, TypeError):
            raise ValueError(
                f"Filename {filename} didn't match 'fpgamap_<numbers>.py'"
            )

        fpga_map = load_map(build, directory)
        return cls(fpga_map=fpga_map)

    def _setup(self, fpga_map: dict):
        """
        :param fpga_map: FPGA address map to be simulated.
        """
        max_word_address = 0
        # find address space and create appropriately sized buffer
        for p_name, peripheral in fpga_map.items():
            max_word_address = max(max_word_address, peripheral["stop"])
        self._values = np.zeros(max_word_address, dtype=ArgsWordType)

        # populate initial values
        for p_name, peripheral in fpga_map.items():
            for s_name, slave in peripheral["slaves"].items():
                if slave["type"] in ["REG", "RAM"]:
                    for f_name, field in slave["fields"].items():
                        field_word_address = (
                            peripheral["start"]
                            + slave["start"]
                            + field["start"]
                        )
                        try:
                            self._values[field_word_address] = field["default"]
                        except KeyError:
                            self._values[field_word_address] = 0

    def _load_firmware(self):
        pass

    def read(self, source, length: int = 1):
        """
        Read values from simulated FPGA register space

        :param source: Byte address to start reading from
        :param length: Number of words to read
        :return: value(s) from simulated registers
        """
        assert source % WORD_SIZE == 0
        word_address = source // WORD_SIZE
        if length == 1:
            return int(self._values[word_address])
        return self._values[word_address : word_address + length]

    def write(self, destination, values):
        """
        Write values to simulated FPGA registers

        :param destination: FPGA byte address where writes should start
        :param values: value(s) to write, if more than one value they must be words,
         to be written to consecutive words (i.e. byte addresses increment by WORD_SIZE)
        """
        assert destination % 4 == 0
        word_address = destination // 4
        if isinstance(values, (int, np.integer)):
            self._values[word_address] = values
        elif isinstance(values, np.ndarray):
            self._values[word_address : word_address + values.size] = values
        else:
            raise TypeError(f"Don't know how to write type {type(values)}")

    def read_memory(self, index: int) -> np.ndarray:
        """
        Read a shared memory buffer.
        :param index: Index of the shared buffer to save.
        Zero is the ARGS interchange buffer, which you probably don't want.
        :return: shared memory buffer
        """
        raise NotImplementedError("Memory buffers not yet simulated")

    def write_memory(self, index: int, values: np.ndarray, offset: int = 0):
        """
        Write to a shared memory buffer.
        :param index: Index of the shared buffer to write to.
        Zero is the ARGS interchange buffer, which you probably don't want.
        :param values: Data to write.
        :param offset: Word-based offset where values should start in buffer.
        """
        raise NotImplementedError("Memory buffers not yet simulated")
