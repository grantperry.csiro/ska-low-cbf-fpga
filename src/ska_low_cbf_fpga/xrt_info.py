# -*- coding: utf-8 -*-
#
# Copyright (c) 2022 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
import json

import pyxrt


class XrtInfo:

    _INFO_PARAMS = {
        "bdf": str,
        "dynamic_regions": "json",
        "electrical": "json",
        "host": "json",
        "interface_uuid": str,
        "kdma": bool,
        "m2m": bool,
        "max_clock_frequency_mhz": int,
        "mechanical": "json",
        "memory": "json",
        "name": str,
        "nodma": bool,
        "offline": bool,
        "pcie_info": "json",
        "platform": "json",
        "thermal": "json",
    }

    def __init__(self, device: pyxrt.device):
        self._device = device

    @property
    def xclbin_uuid(self) -> str:
        return self._device.get_xclbin_uuid().to_string()

    def __getitem__(self, item):
        if item not in self._INFO_PARAMS.keys():
            raise KeyError(f"{item} is not an available health parameter")

        raw = self._device.get_info(getattr(pyxrt.xrt_info_device, item))
        type_ = self._INFO_PARAMS[item]

        if type_ == "json":
            parsed = json.loads(raw)
            # many of these are dicts with one top-level key
            if len(parsed.keys()) == 1:
                parsed = parsed[next(iter(parsed))]
                # and then often a list with one element
                if type(parsed) == list and len(parsed) == 1:
                    parsed = parsed[0]
            return parsed
        elif type(type_) == type:
            if type_ == bool:
                # We need str '0' to evaluate as False
                return bool(int(raw))
            return type_(raw)
        else:
            raise ValueError(f"Unsupported type {self._INFO_PARAMS[item]}")

    def __contains__(self, item):
        return item in self._INFO_PARAMS.keys()

    def __iter__(self):
        return iter(self._INFO_PARAMS.keys())
