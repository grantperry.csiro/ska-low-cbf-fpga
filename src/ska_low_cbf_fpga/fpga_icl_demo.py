# -*- coding: utf-8 -*-
#
# Copyright (c) 2021 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence Agreement
# See LICENSE.txt for more info.

"""
Instrument Control Layer - Experimental!!
"""

import argparse
import logging
import os

from args_fpga import mem_parse, str_from_int_bytes
from args_map import ArgsMap
from args_polled_adapter import ArgsPolledAdapter
from args_simulator import ArgsSimulator
from fpga_icl import FpgaPersonality
from packetiser import Packetiser


class DemoPersonality(FpgaPersonality):
    _peripheral_class = {
        "packetiser": Packetiser,
    }


def main():
    logging.basicConfig(level=logging.DEBUG, filename="fpgacmdline.log")
    my_logger = logging.getLogger()

    # command line arguments
    # designed to be (mostly) compatibility with alveo2gemini
    # we can't use -h (built in, used for help), so -m instead
    parser = argparse.ArgumentParser(description="Alveo ARGS utility")
    parser.add_argument(
        "-f", "--kernel", type=str, help="path to xclbin kernel file"
    )
    parser.add_argument(
        "-d", "--card", default=0, type=int, help="index of card to use"
    )
    parser.add_argument(
        "-m",
        "--memory",
        type=str,
        help="""(HBM) memory configuration <size><unit><s|i>
             size: int
             unit: k, M, G (powers of 1024)
             s: shared
             i: FPGA internal
             e.g. '128Ms:1Gi'""",
    )
    parser.add_argument(
        "-s",
        "--simulate",
        type=str,
        help="path to fpgamap_nnnnnnnn.py file to simulate",
    )
    parser.add_argument(
        "-p", "--polled", action="store_true", help="use polled adapter"
    )
    parser.add_argument(
        "--driver",
        type=str,
        help="Select driver (xrt/cl) [default cl, ignored if simulate set]",
        default="cl",
    )
    args = parser.parse_args()

    if args.kernel:
        logging.info("Kernel: " + args.kernel)
        logging.info("Card: " + str(args.card))
    memory = None
    if args.memory:
        memory = mem_parse(args.memory)
        mem_info = "Memories: "
        for m in memory:
            mem_info += str_from_int_bytes(m.size)
            mem_info += " shared" if m.shared else " internal"
            mem_info += ", "
        logging.info(mem_info[:-2])

    if args.simulate:
        logging.info("Simulating: " + args.simulate)
        driver = ArgsSimulator(fpgamap_path=args.simulate)
        map_dir = os.path.dirname(args.simulate)
    else:
        if not args.kernel:
            raise RuntimeError("Please supply either kernel or simulation map")

        if args.driver.lower() == "xrt":
            import args_xrt

            driver_class = args_xrt.ArgsXrt
        elif args.driver.lower() == "cl":
            import args_cl

            driver_class = args_cl.ArgsCl
        else:
            raise RuntimeError(f"Unknown driver {args.driver}")

        driver = driver_class(
            xcl_file=args.kernel,
            mem_config=memory,
            card=args.card,
            logger=my_logger,
        )
        map_dir = os.path.dirname(args.kernel)
    if args.polled:
        logging.info("Using ArgsPolledAdapter interface")
        interface = ArgsPolledAdapter(driver)
    else:
        interface = driver

    args_map = ArgsMap(driver.get_map_build(), map_dir)
    demo_fpga = DemoPersonality(interface, args_map, logger=logging)

    import code

    code.interact(local=locals())


if __name__ == "__main__":
    main()
