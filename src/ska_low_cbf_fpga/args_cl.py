import threading
import typing

import numpy as np
import pyopencl as cl

from ska_low_cbf_fpga.args_fpga import (
    ARGS_SHARED_ADDRESS,
    EXCHANGE_BUF_CONFIG,
    WORD_SIZE,
    ArgsFpgaDriver,
    ArgsWordType,
    MemConfig,
    mem_parse,
    str_from_int_bytes,
)

"""
FPGA Driver using an OpenCL interface.
You will need to have pyopencl installed!
"""


class ArgsCl(ArgsFpgaDriver):
    """
    pyopencl-based FPGA Driver.
    Requires a .xclbin file.
    """

    def _setup(
        self,
        xcl_file: str,  # TODO use a file object instead?
        mem_config: typing.Union[list, str] = "",
        card: int = 0,
    ):
        """
        :param xcl_file: path to a .xclbin FPGA kernel
        :param mem_config: a list of MemConfig tuples (<size in bytes>, <shared?>)
            the first list item is used to send/receive register values
        :param card: int device index
        """

        self._card = card
        self._mem_config = mem_config
        self._host_buf = []  # numpy buffers for use on host
        self._cl_buf = []  # CL buffer objects
        self._shared_bufs = []
        """List of buffer indexes that are shared between host & FPGA"""
        self._kernel = None
        self._lock = (
            threading.Lock()
        )  # to prevent multiple simultaneous kernel calls
        self._xcl_file = xcl_file

    def _init_cl(self):
        """
        Prepare PyOpenCL infrastructure to communicate with FPGA card
        """
        platforms = cl.get_platforms()
        self.logger.debug("platforms: {}".format(platforms))
        devices = None
        for platform in platforms:
            if platform.vendor == "Xilinx":
                devices = platform.get_devices()
                break
        if not devices:
            raise RuntimeError("Could not find Xilinx CL platform")
        self.logger.debug("{} devices: {}".format(len(devices), devices))

        self._device = [devices[self._card]]
        self.logger.debug("device: {}".format(self._device))
        self._ctx = cl.Context(self._device)
        self._queue = cl.CommandQueue(self._ctx)
        self.logger.debug("queue created - id: {}".format(id(self._queue)))

    def _load_firmware(self):
        """
        Load the binary to the FPGA (if required) and initialise our kernel object
        """
        self._init_cl()

        self.logger.debug("Loading {}".format(self._xcl_file))
        xcl_binary = open(self._xcl_file, "rb").read()
        prg = cl.Program(self._ctx, self._device, [xcl_binary])
        kernel_list = prg.all_kernels()

        for k in kernel_list:
            self.logger.debug(
                "Found kernel {}".format(
                    k.get_info(cl.kernel_info.FUNCTION_NAME)
                )
            )

        kernel = kernel_list[0]
        num_args = kernel.num_args
        self.logger.debug(
            "kernel {} has {} args:".format(kernel.function_name, num_args)
        )
        for arg in range(num_args):
            self.logger.debug(
                "{} {} {}".format(
                    arg,
                    kernel.get_arg_info(arg, cl.kernel_arg_info.NAME),
                    kernel.get_arg_info(arg, cl.kernel_arg_info.TYPE_NAME),
                )
            )
        self._kernel = kernel

        self._init_buffers()

    def _init_buffers(self):
        """
        Create CL & Numpy buffers
        """
        mem_config = self._mem_config
        if (
            mem_config
            and isinstance(mem_config, list)
            and all(isinstance(_, MemConfig) for _ in mem_config)
        ):
            self._mem_config = mem_config
        elif isinstance(mem_config, str):
            self._mem_config = mem_parse(mem_config)
        else:
            self.logger.warning(
                "you should supply a list of MemConfigs - using default"
                "interchange buffer {}".format(EXCHANGE_BUF_CONFIG)
            )
            self._mem_config = [EXCHANGE_BUF_CONFIG]
        mf = cl.mem_flags
        for index, memory in enumerate(self._mem_config):
            if memory.shared:
                self.logger.debug(
                    "allocating {} of shared memory".format(
                        str_from_int_bytes(memory.size)
                    )
                )
                if memory.size % WORD_SIZE:
                    raise ValueError(
                        f"memory size not a multiple of {WORD_SIZE} bytes"
                    )
                else:
                    self._host_buf.append(
                        np.zeros(memory.size // WORD_SIZE).astype(ArgsWordType)
                    )
                self._cl_buf.append(
                    cl.Buffer(
                        self._ctx,
                        mf.READ_WRITE | mf.USE_HOST_PTR,
                        hostbuf=self._host_buf[-1],
                    )
                )
                self._shared_bufs.append(index)
            else:
                self.logger.debug(
                    "allocating {} of device-only memory".format(
                        str_from_int_bytes(memory.size)
                    )
                )
                self._host_buf.append(None)
                self._cl_buf.append(
                    cl.Buffer(self._ctx, mf.HOST_NO_ACCESS, size=memory.size)
                )
        self.EXCHANGE_BUF_BYTES = self._host_buf[0].nbytes

    # TODO move to base class ?
    def read(self, source: int, length: int = 1):
        """
        Read values from FPGA

        :param source: Byte address to start reading from
        :param length: Number of words to read
        :return: value(s) from FPGA
        """
        with self._lock:
            values = np.empty(length, dtype=ArgsWordType)
            for word_offset in range(0, length, self._host_buf[0].size):
                read_address = source + word_offset * WORD_SIZE
                words_remaining = length - word_offset
                valid_words = min(words_remaining, self._host_buf[0].size)
                self._read_args_page(read_address, valid_words)
                values[
                    word_offset : word_offset + valid_words
                ] = self._host_buf[0][:valid_words]
            if length == 1:
                return int(values)
            return values

    def _read_args_page(self, address_start: int, length: int = None):
        """
        Read one block of register values into host memory buffer
        :param address_start: Byte address to start reading from
        :param length: Number of words to read
        """
        src = address_start
        dst = ARGS_SHARED_ADDRESS
        if length is None:
            length = self.EXCHANGE_BUF_BYTES
        else:
            length = min(length * WORD_SIZE, self.EXCHANGE_BUF_BYTES)
        if self._lock.locked():
            # set kernel arguments
            self._kernel.set_args(
                np.uint32(src),
                np.uint32(dst),
                self._cl_buf[0],
                np.uint32(length),
                *self._cl_buf[1:],
            )
            # migrate from host
            cl.enqueue_migrate_mem_objects(self._queue, [self._cl_buf[0]])
            self._queue.finish()
            # execute kernel
            cl.enqueue_nd_range_kernel(
                self._queue, self._kernel, self._host_buf[0].shape, None
            )
            self._queue.finish()
            # migrate to host
            cl.enqueue_migrate_mem_objects(
                self._queue,
                [self._cl_buf[0]],
                flags=cl.mem_migration_flags.HOST,
            )
            self._queue.finish()
        else:
            raise RuntimeError("_read_args_page called without lock")

    def write(self, destination: int, values: typing.Union[int, np.ndarray]):
        """
        Write values to FPGA

        :param destination: FPGA byte address where writes should start
        :param values: value(s) to write, if more than one value they must be words,
         to be written to consecutive words (i.e. byte addresses increment by WORD_SIZE)
        """
        if isinstance(values, (int, np.integer)):
            values = np.array(values, dtype=ArgsWordType, ndmin=1)
        assert values.dtype == ArgsWordType

        with self._lock:
            for word_offset in range(0, values.size, self._host_buf[0].size):
                write_address = destination + word_offset * WORD_SIZE
                words_remaining = values.size - word_offset
                # number of words that are valid to write in this loop iteration
                valid_words = min(words_remaining, self._host_buf[0].size)
                self._write_args_page(
                    write_address,
                    values[word_offset : word_offset + valid_words],
                )

    def _write_args_page(self, destination, values):
        """
        Write one block of register values from host buffer to FPGA
        :param destination: Byte address to start writing to
        :param values: Array of values to be written to consecutive words
        """
        size = np.size(values)
        if self._lock.locked():
            # copy input data into shared buffer
            np.copyto(self._host_buf[0][:size], values)
            cl.enqueue_migrate_mem_objects(self._queue, [self._cl_buf[0]])
            self._queue.finish()
            # set kernel arguments
            self._kernel.set_args(
                np.uint32(ARGS_SHARED_ADDRESS),
                np.uint32(destination),
                self._cl_buf[0],
                np.uint32(values.nbytes),
                *self._cl_buf[1:],
            )
            # execute kernel
            cl.enqueue_nd_range_kernel(
                self._queue, self._kernel, self._host_buf[0].shape, None
            )
            self._queue.finish()
        else:
            raise RuntimeError("_write_args_page called without lock")

    def read_memory(
        self, index: int, size_bytes: int = None, offset_bytes: int = 0
    ) -> np.ndarray:
        """
        Read a shared memory buffer.
        I can't see how to do a partial memory buffer migration in PyOpenCL,
        so this migrates the whole buffer then returns the slice of interest.
        :param size_bytes: number of bytes to transfer
        (transfers the whole buffer if not specified or None)
        :param offset_bytes: starting address
        :param index: Index of the shared buffer to save.
        Zero is the ARGS interchange buffer, which you probably don't want.
        :return: shared memory buffer
        """
        assert (
            index in self._shared_bufs
        ), f"Buffer #{index} is not a shared memory"
        with self._lock:
            cl.enqueue_migrate_mem_objects(
                self._queue,
                [self._cl_buf[index]],
                flags=cl.mem_migration_flags.HOST,
            )
            self._queue.finish()

        if size_bytes:
            return (
                self._host_buf[index]
                .view(dtype=np.uint8)[
                    offset_bytes : (offset_bytes + size_bytes)
                ]
                .view(ArgsWordType)
            )
        return self._host_buf[index]

    def write_memory(
        self, index: int, values: np.ndarray, offset_bytes: int = 0
    ):
        """
        Write to a shared memory buffer.
        :param index: Index of the shared buffer to write to.
        Zero is the ARGS interchange buffer, which you probably don't want.
        :param values: Data to write.
        :param offset_bytes: Byte-based offset where values should start in
        buffer.
        """
        assert (
            index in self._shared_bufs
        ), f"Buffer #{index} is not a shared memory"
        with self._lock:
            self._host_buf[index].view(dtype=np.uint8)[
                offset_bytes : offset_bytes + values.size
            ] = values
            cl.enqueue_migrate_mem_objects(
                self._queue,
                [self._cl_buf[index]],
            )
            self._queue.finish()
