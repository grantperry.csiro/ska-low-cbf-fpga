# -*- coding: utf-8 -*-
#
# (c) 2021 CSIRO Astronomy and Space.
#
# Distributed under the terms of the CSIRO Open Source Software Licence Agreement
# See LICENSE.txt for more info.

"""
Xilinx Alveo FPGA acclerator card command-line ARGS register access utility.
"""

import argparse
import logging
import os
from collections import defaultdict, namedtuple
from time import localtime, strftime, time

from blessed import Terminal
from IPython import embed

import ska_low_cbf_fpga.register_load as register_load
from ska_low_cbf_fpga.args_fpga import mem_parse, str_from_int_bytes
from ska_low_cbf_fpga.args_map import ArgsMap
from ska_low_cbf_fpga.fpga_icl import FpgaPersonality


class UserInterface:
    TOP_ROW_OFFSET = 3  # note heading is printed 2 rows above top of table
    LEFT_OFFSET = 3
    WriteLogEntry = namedtuple(
        "WriteLogEntry", ["time", "peripheral", "register", "value"]
    )
    HELP_KEYS = {
        "H": "Display Help",
        "Q": "Quit",
        "X": "Toggle Hexadecimal Display",
        "Up/Down": "Select FPGA register",
        "Enter": "Write a value to FPGA register",
    }

    def __init__(self, fpga, card, kernel):
        self.term = Terminal()
        self.fpga = fpga
        self.card = card
        self.kernel = kernel
        self.cur_x = 0
        self.cur_y = 0
        self.write_log = []
        self.max_name_len = max(
            [
                len(reg_name)
                for peri in fpga.peripherals
                for reg_name in fpga[peri]._fields.keys()
            ]
        )
        self.max_help_key_len = max(len(key) for key in self.HELP_KEYS.keys())
        self.max_help_dsc_len = max(
            len(val) for val in self.HELP_KEYS.values()
        )
        # in future _rows/cols could be modified for multi-column display
        self.n_rows = sum(
            [len(fpga[peri]._fields.keys()) for peri in fpga.peripherals]
        )
        self.n_cols = 1
        self.cache = defaultdict(lambda: None)
        self.table_bottom = None
        self.entry_val = ""
        self.entry_mode = False
        self.hex_mode = False
        self.help_mode = False
        self.loop = False
        self.selected = (None, None)  # (peripheral, register [, index])
        self.fresh = defaultdict(lambda: False)

    def render_loop(self):
        self.loop = True

        with self.term.fullscreen(), self.term.cbreak(), self.term.hidden_cursor():
            while self.loop:
                self.update_cache()
                self.prune_log(20)

                print(self.term.clear)
                self.render_headline()
                self.render_registers()
                self.render_log()
                if self.help_mode:
                    self.render_help()
                if self.entry_mode:
                    self.render_entry()
                self.render_status()

                self.process_input(self.term.inkey(timeout=5))

    def update_cache(self):
        for peri in self.fpga.peripherals:
            for name, reg in self.fpga[peri]._fields.items():
                value = reg.value
                # TODO new "fresh" logic
                # if isinstance(reg, FpgaRam):
                #    self.fresh[name] = any(value != self.cache[name])
                # elif isinstance(reg, FpgaRegister):
                #    self.fresh[name] = value != self.cache[name]
                # else:
                #    raise NotImplementedError("Unknown register data type")
                self.cache[name] = value

    def prune_log(self, timeout):
        # remove stale log entries
        while self.write_log and self.write_log[-1].time < time() - timeout:
            self.write_log.pop()

    def process_input(self, key):
        term = self.term
        if key:
            if self.entry_mode:
                # filter out multi-byte sequences & control characters
                if not key.is_sequence and ord(key) > ord(" "):
                    self.entry_val += key
                elif key.code == term.KEY_BACKSPACE:
                    self.entry_val = self.entry_val[:-1]
                elif key.code == term.KEY_ENTER:
                    self.entry_mode = False
                    peri = self.selected[0]
                    reg = self.selected[1]
                    try:
                        index = self.selected[2]
                    except IndexError:
                        # this implies we have an FpgaRegister, not FpgaRam
                        index = None
                    type_ = self.fpga[peri][reg].type
                    if type_ == int:
                        # base 0 means detect base from string (0x, 0b)
                        write_val = int(self.entry_val, base=0)
                    else:
                        write_val = self.entry_val
                    if index is None:
                        self.fpga[peri][reg] = write_val
                        log_reg = reg
                    else:
                        # FpgaRam
                        self.fpga[peri][reg][index] = write_val
                        log_reg = f"{peri}[{index}]"

                    self.write_log.insert(
                        0,
                        UserInterface.WriteLogEntry(
                            time(), reg, log_reg, self.entry_val
                        ),
                    )
                # clean-up when finished with entry
                if not self.entry_mode:
                    self.entry_val = ""
            else:
                if key.code == term.KEY_ENTER:
                    self.help_mode = False
                    self.entry_mode = True
                    if self.hex_mode:
                        self.entry_val = "0x"
                if key.code == term.KEY_UP and self.cur_y > 0:
                    self.cur_y -= 1
                elif (
                    key.code == term.KEY_DOWN and self.cur_y < self.n_rows - 1
                ):
                    self.cur_y += 1
                elif key.code == term.KEY_LEFT and self.cur_x > 0:
                    self.cur_x -= 1
                elif (
                    key.code == term.KEY_RIGHT and self.cur_x < self.n_cols - 1
                ):
                    self.cur_x += 1
                elif key.lower() == "q":
                    self.loop = False
                elif key.lower() == "x":
                    self.hex_mode = not self.hex_mode
                elif key.lower() == "h":
                    self.help_mode = not self.help_mode
            if key.code == term.KEY_ESCAPE:
                self.help_mode = False
                self.entry_mode = False

    def render_headline(self):
        heading = self.term.ljust(
            "FPGA Registers", width=self.term.width // 2 - self.LEFT_OFFSET
        )
        if self.write_log:
            heading += "Write Log"
        with self.term.location(x=self.LEFT_OFFSET, y=self.TOP_ROW_OFFSET - 2):
            print(self.term.cyan(heading))

    def render_registers(self):
        term = self.term
        row = 0
        row_min = max(self.cur_y - 2, 0)
        row_max = min(self.cur_y + 2, self.n_rows)
        for peri in self.fpga.peripherals:
            for name, reg in self.fpga[peri]._fields.items():
                if row < row_min:
                    row += 1
                    continue
                if row > row_max:
                    break
                selected = row == self.cur_y
                y_pos = row + self.TOP_ROW_OFFSET - row_min
                fresh = self.fresh[name]
                if selected:
                    self.selected = (peri, name)
                value = reg.value
                with term.location(x=self.LEFT_OFFSET, y=y_pos):
                    if fresh and selected:
                        print(term.bold_black_on_yellow(name))
                    elif selected:
                        print(term.black_on_white(name))
                    elif fresh:
                        print(term.bold_yellow(name))
                    else:
                        print(name)
                with term.location(
                    x=self.max_name_len + self.LEFT_OFFSET + 1, y=y_pos
                ):
                    if self.hex_mode:
                        value = hex(value)
                    else:
                        value = str(value)
                    if fresh:
                        print(term.bold_yellow(value))
                    else:
                        print(value)
                if selected and self.entry_mode:
                    with term.location(x=0, y=y_pos):
                        print(term.bold_red("*"))
                    with term.location(
                        x=self.max_name_len + len(str(value)) + 5, y=y_pos
                    ):
                        print(term.bold_red("*"))
                row += 1
        self.table_bottom = row

    def render_log(self):
        term = self.term
        x_log = self.term.width // 2
        y_pos = self.TOP_ROW_OFFSET
        for w in self.write_log:
            with term.location(x=x_log, y=y_pos):
                print(
                    term.dimgrey(
                        "{} {}.{} <= {}".format(
                            strftime("%H:%M:%S", localtime(w.time)),
                            w.peripheral,
                            w.register,
                            w.value,
                        )
                    )
                )
            y_pos += 1

    def render_status(self):
        term = self.term
        with term.location(x=0, y=term.height - 3):
            print(
                f"{term.dimgrey}Card: {term.grey}{self.card:2}{term.dimgrey}"
                f" Kernel: {self.kernel}"
            )
        with term.location(x=0, y=term.height - 2):
            timestr = term.dimgrey(strftime("%H:%M:%S", localtime()))
            if self.entry_mode:
                keystr = term.ljust(
                    term.cyan_bold("(Esc): Cancel"), width=term.width // 2
                )
            else:
                keystr = term.ljust(
                    term.cyan_bold("(Q)uit (H)elp"), width=term.width // 2
                )
            print(keystr + timestr)

    def render_entry(self):
        # new value entry prompt
        term = self.term
        y_pos = self.table_bottom + self.TOP_ROW_OFFSET + 2
        with term.location(x=2, y=y_pos):
            if len(self.selected) == 2:
                # FpgaRegister
                print(
                    f"{term.bold_cyan}Set {self.selected[0]}.{self.selected[1]}:"
                    f" {term.normal}{self.entry_val}"
                )
            elif len(self.selected) == 3:
                # FpgaRam
                print(
                    f"{term.bold_cyan}Set {self.selected[0]}.{self.selected[1]}"
                    f"[{self.selected[2]}]: {term.normal}{self.entry_val}"
                )

    def render_help(self):
        term = self.term
        y_pos = (term.height // 2) - (len(self.HELP_KEYS) // 2)
        x_pos = (term.width // 2) - (
            self.max_help_dsc_len + self.max_help_key_len + 1
        ) // 2
        for key, desc in self.HELP_KEYS.items():
            with term.location(y=y_pos, x=x_pos):
                key = term.center(key, self.max_help_key_len)
                print(f"{term.bold_cyan}{key} {term.normal}{desc}")
                y_pos += 1


def main():
    logging.basicConfig(level=logging.DEBUG, filename="fpgacmdline.log")
    my_logger = logging.getLogger()
    # command line arguments
    # designed to be (mostly) compatibility with alveo2gemini
    # we can't use -h (built in, used for help), so -m instead
    parser = argparse.ArgumentParser(description="Alveo ARGS utility")
    parser.add_argument(
        "-f",
        "--kernel",
        type=str,
        help="path to xclbin kernel file",
    )
    parser.add_argument(
        "-d",
        "--cards",
        default=[0],
        type=int,
        help="indexes of cards to use",
        nargs="+",
    )
    parser.add_argument(
        "-m",
        "--memory",
        type=str,
        help="""(HBM) memory configuration <size><unit><s|i>
             size: int
             unit: k, M, G (powers of 1024)
             s: shared
             i: FPGA internal
             e.g. '128Ms:1Gi'""",
    )
    parser.add_argument(
        "--driver",
        type=str,
        help="Select driver (xrt/cl) [default cl, ignored if simulate set]",
        default="cl",
    )
    parser.add_argument(
        "-s",
        "--simulate",
        type=str,
        help="path to fpgamap_nnnnnnnn.py file to simulate",
    )
    parser.add_argument(
        "-r",
        "--registers",
        type=argparse.FileType(mode="r"),
        help="register setting text file to load",
    )
    parser.add_argument(
        "-i",
        "--interactive",
        action="store_true",
        help="use interactive interface",
    )
    parser.add_argument(
        "-c", "--console", action="store_true", help="use IPython console"
    )
    args = parser.parse_args()
    print("Cards:", args.cards)
    if args.kernel:
        logging.info("Kernel: " + args.kernel)
        logging.info("Cards: " + str(args.cards))
    memory = None
    if args.memory:
        memory = mem_parse(args.memory)
        mem_info = "Memories: "
        for m in memory:
            mem_info += str_from_int_bytes(m.size)
            mem_info += " shared" if m.shared else " internal"
            mem_info += ", "
        logging.info(mem_info[:-2])
    if args.simulate:
        from ska_low_cbf_fpga.args_simulator import ArgsSimulator

        logging.info("Simulating: " + args.simulate)
        drivers = {
            card: ArgsSimulator(fpgamap_path=args.simulate)
            for card in args.cards
        }
        map_dir = os.path.dirname(args.simulate)
    else:
        if not args.kernel:
            raise RuntimeError("Please supply either kernel or simulation map")
        map_dir = os.path.dirname(args.kernel)

        if args.driver.lower() == "xrt":
            import ska_low_cbf_fpga.args_xrt

            driver_class = ska_low_cbf_fpga.args_xrt.ArgsXrt
        elif args.driver.lower() == "cl":
            import ska_low_cbf_fpga.args_cl

            driver_class = ska_low_cbf_fpga.args_cl.ArgsCl
        else:
            raise RuntimeError(f"Unknown driver {args.driver}")

        drivers = {
            card: driver_class(
                xcl_file=args.kernel,
                mem_config=memory,
                card=card,
                logger=my_logger,
            )
            for card in args.cards
        }

    fpgas = {
        card: FpgaPersonality(
            driver,
            ArgsMap.create_from_file(driver.get_map_build(), map_dir),
            logger=my_logger,
        )
        for card, driver in drivers.items()
    }
    if args.registers:
        for fpga in fpgas.values():
            register_load.load(fpga, args.registers.read())
    if args.interactive:
        if len(args.cards) > 1:
            raise NotImplementedError("Only 1 FPGA card supported for CLI")
        card = args.cards[0]
        ui = UserInterface(fpgas[card], card, args.kernel)
        ui.render_loop()
    elif args.console:
        if len(args.cards) == 1:
            # shortcut for user convenience when working with one card
            fpga = fpgas[args.cards[0]]
        embed(colors="neutral")


if __name__ == "__main__":
    main()
