# -*- coding: utf-8 -*-
#
# Copyright (c) 2021 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence Agreement
# See LICENSE.txt for more info.

"""
Base classes to interface with ARGS-based FPGA
"""

import logging
import typing
from abc import ABC, abstractmethod
from dataclasses import dataclass
from math import log2

import numpy as np

ArgsWordType = np.uint32  # must match internal FPGA addressing scheme
WORD_SIZE = ArgsWordType(1).nbytes
ARGS_SHARED_ADDRESS = 0x8000 * WORD_SIZE
ARGS_MAGIC = 0xF96A7001
ARGS_MAGIC_ADDRESS = 0 * WORD_SIZE
ARGS_BUILD_ADDRESS = 1 * WORD_SIZE


@dataclass
class MemConfig:
    """Memory buffer configuration info"""

    size: int  # in bytes
    shared: bool  # False = FPGA internal


EXCHANGE_BUF_CONFIG = MemConfig(128 << 10, True)


def str_from_int_bytes(n_bytes: int) -> str:
    """
    Automatically scale a number of bytes for printing
    :param n_bytes: Number of bytes
    :return: A number and units, e.g. '1.5 KiB'
    """
    unit = int(log2(n_bytes)) // 10
    scaled = n_bytes / 2 ** (unit * 10)

    # IEC 80000 standard prefixes for multiple-byte units by powers of 1024
    units = ["B", "KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB", "YiB"]
    if scaled % 1 == 0:
        # convert to int to avoid printing ".0"
        scaled = int(scaled)
    return str(scaled) + " " + units[unit]


def mem_parse(memory_config: str = "") -> typing.List[MemConfig]:
    """
    Takes a string-formatted list of memory configurations and converts to MemConfig.
    The register interchange buffer is automatically included as the first element.
    Do not list the register buffer in the input string!

    :param memory_config: a colon-separated string
        <size><unit><s|i>
         size: int
         unit: k, M, G (powers of 1024)
         s: shared
         i: FPGA internal
         e.g. '128Ms:1Gi'
    :return: list of MemConfigs, decoded from the string
    """
    mem = [EXCHANGE_BUF_CONFIG]
    memories = memory_config.split(":")
    for memory in memories:
        if len(memory) == 0:
            continue

        if memory[-1] == "s":
            shared = True
        elif memory[-1] == "i":
            shared = False
        else:
            continue

        if memory[-2] == "k":
            unit = 1 << 10
        elif memory[-2] == "M":
            unit = 1 << 20
        elif memory[-2] == "G":
            unit = 1 << 30
        else:
            continue

        size = int(memory[:-2])
        mem.append(MemConfig(size * unit, shared))
    return mem


class ArgsFpgaInterface:
    """
    Common FPGA Interface definition. Derived classes must provide implementation.
    """

    @abstractmethod
    def read(self, source, length=None):
        """
        Read from FPGA register(s)
        :param source: Start address (bytes)
        :param length: Number of words to read
        """
        raise NotImplementedError

    @abstractmethod
    def write(self, destination, values):
        """
        Write to FPGA register(s)
        :param destination: Start address (bytes)
        :param values: One or more values to write to consecutive words
        """
        raise NotImplementedError

    @abstractmethod
    def read_memory(
        self, index: int, size_bytes: int = None, offset_bytes: int = 0
    ) -> np.ndarray:
        """
        Read a shared memory buffer.
        :param size_bytes: number of bytes to transfer
        (transfers the whole buffer if not specified or None)
        :param offset_bytes: starting address
        :param index: Index of the shared buffer to save.
        Zero is the ARGS interchange buffer, which you probably don't want.
        :return: shared memory buffer
        """
        raise NotImplementedError

    @abstractmethod
    def write_memory(
        self, index: int, values: np.ndarray, offset_bytes: int = 0
    ):
        """
        Write to a shared memory buffer.
        :param index: Index of the shared buffer to write to.
        Zero is the ARGS interchange buffer, which you probably don't want.
        :param values: Data to write.
        :param offset_bytes: Byte-based offset where values should start in
        buffer.
        """
        raise NotImplementedError


class ArgsFpgaDriver(ArgsFpgaInterface, ABC):
    """
    Base class for ARGS FPGA Drivers.
    Defines some common startup steps and interfaces.
    """

    def __init__(self, logger=None, **kwargs):
        """
        :param logger: a logging object with .debug, .info, etc functions
        :param kwargs: optional extra arguments passed on to the derived class's _init
        function
        """
        if logger is None:
            self.logger = logging.getLogger()
        else:
            self.logger = logger
        self._setup(**kwargs)
        self._load_firmware()
        self._check_magic()

    @abstractmethod
    def _setup(self, *args, **kwargs):
        """Derived class to provide initialisation code"""

    @abstractmethod
    def _load_firmware(self):
        """Derived class to provide firmware loading code"""

    def _check_magic(self):
        """
        Check that FPGA contains the correct 'Magic Number'
        """
        magic_number = self.read(ARGS_MAGIC_ADDRESS)
        if not magic_number:
            raise RuntimeError("No ARGS magic number from FPGA")
        if magic_number != ARGS_MAGIC:
            msg = "ARGS Magic Number mismatch! expected: 0x{:x}, got: 0x{:x}".format(
                ARGS_MAGIC, magic_number
            )
            raise RuntimeError(msg)

    def get_map_build(self):
        """
        Get the ARGS map build timestamp from the FPGA
        """
        map_build = self.read(ARGS_BUILD_ADDRESS)
        if not map_build:
            raise RuntimeError("No ARGS map build from FPGA")
        return int(map_build)


class ArgsAdapter(ArgsFpgaInterface, ABC):
    """
    Base FPGA 'adapter' class, derived classes can implement buffering or similar.
    """

    def __init__(self, fpga_driver: ArgsFpgaDriver):
        if not isinstance(fpga_driver, ArgsFpgaDriver):
            raise TypeError("fpga_driver must be an ArgsFpgaDriver")
        self._driver = fpga_driver
