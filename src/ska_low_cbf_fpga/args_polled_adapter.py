import bisect
import threading

import numpy as np
from args_fpga import (
    EXCHANGE_BUF_CONFIG,
    WORD_SIZE,
    ArgsAdapter,
    ArgsFpgaDriver,
    ArgsWordType,
)

BLOCK_WORDS = EXCHANGE_BUF_CONFIG.size // WORD_SIZE

"""
An FPGA Interface (Adapter) that implements periodic polling.
"""


class ArgsPolledAdapter(ArgsAdapter):
    """ "
    ARGS FPGA Adapter for periodic polling.
    All writes & reads are performed periodically.
    FPGA writes will update the read cache.
    Every FPGA address that has ever been requested will be read every poll.

    WARNINGS:
        First read of any address will return 'None', subsequent reads before the
    polling loop is executed will return 0.
        Write ordering is not guaranteed.
    """

    def __init__(self, fpga_driver: ArgsFpgaDriver, sleep_time: int = 3):
        super().__init__(fpga_driver)
        self._sleep_time = sleep_time
        """wait time (seconds) after each polling loop"""
        self._buffer = np.zeros(BLOCK_WORDS, dtype=ArgsWordType)
        """key: WORD address, value: updated each poll, defaults to None"""

        self._max_address = self._buffer.nbytes
        self._active_read_addresses = [0]  # in bytes
        self._write_buf = {}
        """key: BYTE address"""
        self._exit_event = threading.Event()
        self._thread = threading.Thread(target=self.poll, daemon=True)
        self._thread.start()

    def __del__(self):
        self._exit_event.set()
        self._thread.join()

    def read(self, source, length: int = 1):
        """
        :return: None on first access! Zero if read again before first poll!
        Value from last poll otherwise.
        """
        # these addresses are in bytes
        start_address = source
        end_address = start_address + length * WORD_SIZE

        if start_address not in self._active_read_addresses:
            for address in range(start_address, end_address, WORD_SIZE):
                bisect.insort(self._active_read_addresses, address)

        if end_address > self._max_address:
            self.update_max_address(end_address)
            return None

        if length == 1:
            return int(self._buffer[start_address // WORD_SIZE])
        return self._buffer[
            start_address // WORD_SIZE : end_address // WORD_SIZE
        ]

    def write(self, destination, values):
        """
        Cache value to be written. If read back, the cached value will be returned.
        """
        # add to list of writes to be performed
        self._write_buf[destination] = values
        # update read cache too, higher levels will be expecting it to have changed
        self._buffer[destination // WORD_SIZE] = values

    def poll(self):
        """Background polling loop"""
        while True:
            # perform any queued writes - TODO in blocks
            while self._write_buf:
                (address, value) = self._write_buf.popitem()
                self._driver.write(address, value)

            # extract the start addresses we need for each block read
            block_starts = [self._active_read_addresses[0]]
            for address in self._active_read_addresses:
                if address > block_starts[-1] + EXCHANGE_BUF_CONFIG.size - 1:
                    block_starts.append(address)
            # read all the blocks
            for block_start in block_starts:
                driver_read = self._driver.read(block_start, BLOCK_WORDS)
                buffer_start = block_start // WORD_SIZE
                if buffer_start + BLOCK_WORDS > self._buffer.size:
                    driver_words = self._buffer.size - buffer_start
                    self._buffer[buffer_start:] = driver_read[:driver_words]
                else:
                    buffer_end = buffer_start + BLOCK_WORDS
                    self._buffer[buffer_start:buffer_end] = driver_read

            # sleep
            if self._exit_event.wait(timeout=self._sleep_time):
                break

    def update_max_address(self, new_address):
        """
        Update internal buffers
        :param new_address: in bytes
        """
        # an optimization could be done here to skip over unused regions...
        # for now, we just allocate enough memory to read every address from zero!
        n_extra_blocks = int(
            np.ceil(
                (new_address // WORD_SIZE - self._buffer.size) / BLOCK_WORDS
            )
        )
        self._buffer = np.concatenate(
            (
                self._buffer,
                np.zeros(BLOCK_WORDS * n_extra_blocks, dtype=ArgsWordType),
            ),
            axis=0,
        )
        self._max_address = self._buffer.nbytes
