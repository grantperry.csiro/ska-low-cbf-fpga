# -*- coding: utf-8 -*-
#
# Copyright (c) 2021 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence Agreement
# See LICENSE.txt for more info.

import pytest

from ska_low_cbf_fpga.args_fpga import (
    ARGS_BUILD_ADDRESS,
    ARGS_MAGIC,
    ARGS_MAGIC_ADDRESS,
    EXCHANGE_BUF_CONFIG,
    ArgsAdapter,
    ArgsFpgaInterface,
    MemConfig,
    mem_parse,
    str_from_int_bytes,
)

from .conftest import MockDriver


class TestStrBytes:
    @pytest.mark.parametrize(
        "n, expected",
        [
            (1, "1 B"),
            (2048, "2 KiB"),
            (3 << 20, "3 MiB"),
            (4 << 30, "4 GiB"),
            (5 << 40, "5 TiB"),
            (6 << 50, "6 PiB"),
            (7 << 60, "7 EiB"),
            (8 << 70, "8 ZiB"),
            (9 << 80, "9 YiB"),
        ],
    )
    def test_str_from_int_bytes(self, n, expected):
        assert str_from_int_bytes(n) == expected


class TestMemParse:
    def test_default(self):
        assert mem_parse()[0] == EXCHANGE_BUF_CONFIG

    @pytest.mark.parametrize(
        "input_, output",
        [
            ("1ki", (1024, False)),
            ("2ks", (2048, True)),
            ("3Mi", (3 << 20, False)),
            ("4Ms", (4 << 20, True)),
            ("5Gi", (5 << 30, False)),
            ("6Gs", (6 << 30, True)),
        ],
    )
    def test_parse_single(self, input_, output):
        assert mem_parse(input_)[1] == MemConfig(*output)

    def test_parse_multi(self):
        in_str = "3ks:1Mi:2Gs"
        expected = [
            EXCHANGE_BUF_CONFIG,
            MemConfig(3 << 10, True),
            MemConfig(1 << 20, False),
            MemConfig(2 << 30, True),
        ]
        assert mem_parse(in_str) == expected


class TestDriver:
    def test_init_load_called(self):
        with pytest.raises(RuntimeError):
            mock_driver = MockDriver(read_value=None)
            assert mock_driver.init_called
            assert mock_driver.load_firmware_called

    def test_magic_requested(self):
        mock_driver = MockDriver()
        assert ARGS_MAGIC_ADDRESS in mock_driver.read_log

    @pytest.mark.parametrize("fpga_magic", [None, 1234, ARGS_MAGIC + 1])
    def test_bad_magic(self, fpga_magic):
        with pytest.raises(RuntimeError) as bad_magic:
            MockDriver(read_value=fpga_magic)
        assert "args magic number" in str(bad_magic.value).lower()

    def test_map_build(self):
        mock_driver = MockDriver()
        assert mock_driver.read_log.count(ARGS_BUILD_ADDRESS) == 0
        # note same value returned by mock for every address...
        assert mock_driver.get_map_build() == ARGS_MAGIC
        assert mock_driver.read_log.count(ARGS_BUILD_ADDRESS) == 1

    def test_inherits_interface(self):
        mock_driver = MockDriver()
        assert isinstance(mock_driver, ArgsFpgaInterface)


class TestAdapter:
    def test_type_check(self):
        with pytest.raises(Exception):
            ArgsAdapter(1)

    def test_create(self):
        mock_driver = MockDriver()
        adapter = ArgsAdapter(mock_driver)
        # derived classes will be counting on this assignment...
        assert adapter._driver == mock_driver

    def test_inherits_interface(self):
        mock_driver = MockDriver()
        adapter = ArgsAdapter(mock_driver)
        assert isinstance(adapter, ArgsFpgaInterface)
