# -*- coding: utf-8 -*-
#
# Copyright (c) 2021 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence Agreement
# See LICENSE.txt for more info.
from unittest.mock import Mock, patch

import numpy as np
import pytest

from ska_low_cbf_fpga import ArgsFpgaInterface
from ska_low_cbf_fpga.args_fpga import WORD_SIZE, ArgsWordType
from ska_low_cbf_fpga.fpga_icl import (
    DISCOVER_ALL,
    DISCOVER_PROPERTIES,
    DISCOVER_REGISTERS,
    FpgaPeripheral,
    FpgaPersonality,
    IclField,
    IclFpgaBitfield,
    IclFpgaField,
)

from .conftest import DummyArgsMap, MockDriver, my_fields


@pytest.fixture
def icl_test_int_field():
    yield IclField[int](description="Testing", value=42, type_=int)


@pytest.fixture
def icl_test_fpga_field():
    class FakeInterface(ArgsFpgaInterface):
        def __init__(self, *args, **kwargs):
            self.x = 0

        def read(self, source, length=None):
            return self.x

        def write(self, destination, values):
            self.x = np.array(values, ndmin=1)[0]

        def read_memory(self, index: int) -> np.ndarray:
            pass

        def write_memory(
            self, index: int, values: np.ndarray, offset: int = 0
        ):
            pass

    fake_interface = FakeInterface()
    field = IclFpgaField(
        description="Testing",
        read_interface=fake_interface,
        write_interface=fake_interface,
    )
    field.value = 42
    yield field


@pytest.fixture
def icl_test_fpga_array_field():
    class FakeInterface(ArgsFpgaInterface):
        def __init__(self, *args, **kwargs):
            self.x = np.array([0, 1, 2, 3], dtype=ArgsWordType)

        def read(self, source, length=None):
            start = source // WORD_SIZE
            if length is None:
                return self.x[start]
            else:
                return self.x[start : start + length]

        def write(self, destination, values):
            new_values = np.array(values, ndmin=1, dtype=ArgsWordType)
            start = destination // WORD_SIZE
            end = start + len(new_values)
            self.x[start:end] = new_values

        def read_memory(self, index: int) -> np.ndarray:
            pass

        def write_memory(
            self, index: int, values: np.ndarray, offset: int = 0
        ):
            pass

    fake_interface = FakeInterface()
    field = IclFpgaField(
        description="Testing",
        read_interface=fake_interface,
        write_interface=fake_interface,
        address=0,
        length=4,
    )
    yield field


class TestIclField:
    @pytest.mark.usefixtures("icl_test_int_field")
    def test_int_cast(self, icl_test_int_field):
        assert int(icl_test_int_field) == 42

    @pytest.mark.usefixtures("icl_test_int_field")
    def test_integer_arithmetic(self, icl_test_int_field):
        assert icl_test_int_field + 1 == 43
        assert icl_test_int_field - 1 == 41
        assert icl_test_int_field * 2 == 84
        assert icl_test_int_field ** 2 == 1764
        assert icl_test_int_field / 5 == 8.4
        assert icl_test_int_field // 5 == 8
        assert icl_test_int_field % 5 == 2

    @pytest.mark.usefixtures("icl_test_int_field")
    def test_reverse_integer_arithmetic(self, icl_test_int_field):
        two = IclField[int](description="Two", value=2, type_=int)
        assert 1 + icl_test_int_field == 43
        assert 50 - icl_test_int_field == 8
        assert 2 * icl_test_int_field == 84
        assert 42 ** two == 1764
        assert 5 / two == 2.5
        assert 5 // two == 2
        assert 5 % two == 1

    @pytest.mark.usefixtures("icl_test_int_field")
    def test_field_arithmetic(self, icl_test_int_field):
        two = IclField[int](description="Two", value=2, type_=int)
        assert icl_test_int_field + two == 44
        assert icl_test_int_field - two == 40
        assert icl_test_int_field * two == 84
        assert icl_test_int_field ** two == 1764
        assert icl_test_int_field / (two * two) == 10.5
        assert icl_test_int_field // two == 21
        assert icl_test_int_field % two == 0

    @pytest.mark.usefixtures("icl_test_int_field")
    def test_integer_bit_manipulation(self, icl_test_int_field):
        assert icl_test_int_field << 1 == 84
        assert icl_test_int_field >> 1 == 21
        assert icl_test_int_field & 0b1111 == 0b1010
        assert icl_test_int_field | 0b1111 == 47
        assert icl_test_int_field ^ 0b1111 == 37
        assert ~icl_test_int_field == -43

    def test_reverse_integer_bit_manipulation(self):
        one = IclField[int](description="One", value=1, type_=int)
        fifteen = IclField[int](description="Fifteen", value=15, type_=int)
        assert 42 << one == 84
        assert 42 >> one == 21
        assert 42 & fifteen == 0b1010
        assert 42 | fifteen == 47
        assert 42 ^ fifteen == 37

    @pytest.mark.usefixtures("icl_test_int_field")
    def test_field_bit_manipulation(self, icl_test_int_field):
        one = IclField[int](description="One", value=1, type_=int)
        fifteen = IclField[int](description="Fifteen", value=15, type_=int)
        assert icl_test_int_field << one == 84
        assert icl_test_int_field >> one == 21
        assert icl_test_int_field & fifteen == 0b1010
        assert icl_test_int_field | fifteen == 47
        assert icl_test_int_field ^ fifteen == 37

    @pytest.mark.usefixtures("icl_test_int_field")
    def test_integer_comparison(self, icl_test_int_field):
        assert icl_test_int_field == 42
        assert icl_test_int_field != 0
        assert icl_test_int_field >= 42
        assert icl_test_int_field > 41
        assert icl_test_int_field <= 42
        assert icl_test_int_field < 43

    @pytest.mark.usefixtures("icl_test_fpga_field")
    def test_fpga_integer_comparison(self, icl_test_fpga_field):
        assert icl_test_fpga_field == 42
        assert icl_test_fpga_field != 0
        assert icl_test_fpga_field >= 42
        assert icl_test_fpga_field > 41
        assert icl_test_fpga_field <= 42
        assert icl_test_fpga_field < 43

    @pytest.mark.usefixtures("icl_test_int_field")
    def test_field_comparison(self, icl_test_int_field):
        forty_one = IclField[int](description="Forty One", value=41, type_=int)
        assert not icl_test_int_field == forty_one
        assert icl_test_int_field != forty_one
        assert icl_test_int_field >= forty_one
        assert icl_test_int_field > forty_one
        assert forty_one <= icl_test_int_field
        assert forty_one < icl_test_int_field

    def test_bool_field(self):
        true_field = IclField[bool](description="true", value=True, type_=bool)
        false_field = IclField[bool](
            description="false", value=False, type_=bool
        )
        assert true_field
        assert not false_field
        assert not true_field == false_field

    @pytest.mark.usefixtures("icl_test_fpga_array_field")
    def test_array_read(self, icl_test_fpga_array_field):
        for _ in range(4):
            assert icl_test_fpga_array_field[_] == _

    @pytest.mark.usefixtures("icl_test_fpga_array_field")
    def test_array_write(self, icl_test_fpga_array_field):
        for _ in range(4):
            icl_test_fpga_array_field[_] = 5
            assert icl_test_fpga_array_field[_] == 5

    @pytest.mark.usefixtures("icl_test_fpga_array_field")
    def test_array_index_error(self, icl_test_fpga_array_field):
        with pytest.raises(IndexError):
            icl_test_fpga_array_field[4]
        with pytest.raises(IndexError):
            icl_test_fpga_array_field[4] = 4

    @pytest.mark.usefixtures("icl_test_fpga_array_field")
    def test_array_negative_index(self, icl_test_fpga_array_field):
        with pytest.raises(NotImplementedError):
            icl_test_fpga_array_field[-1]
        with pytest.raises(NotImplementedError):
            icl_test_fpga_array_field[-1] = -1


class TestFpgaPeripheral:
    @pytest.mark.parametrize(
        "interfaces",
        [
            1,
            "invalid",
            {"__default__": 3, "other": MockDriver()},
            {
                "__default__": MockDriver(),
                4: MockDriver(),
            },
            # below would be valid *if* we also set the default_interface parameter
            {"no_default": MockDriver()},
        ],
    )
    def test_interfaces_type_check(self, interfaces):
        """Passing invalid interface(s) should fail"""
        with pytest.raises(Exception):
            FpgaPeripheral(interfaces, my_fields)

    @pytest.mark.usefixtures("mock_driver")
    def test_init_single_interface(self, mock_driver):
        FpgaPeripheral(mock_driver, my_fields)

    @pytest.mark.parametrize(
        "interfaces",
        [
            {"__default__": MockDriver()},
            {
                "__default__": MockDriver(),
                "two": MockDriver(),
            },
        ],
    )
    def test_init_interface_dict(self, interfaces):
        FpgaPeripheral(interfaces, my_fields)

    def test_init_change_default_interface(self):
        my_interface = MockDriver()
        peripheral = FpgaPeripheral(
            {"unusual_default": my_interface, "__default__": MockDriver()},
            my_fields,
            default_interface="unusual_default",
        )
        assert peripheral["my_field"].read_interface == my_interface
        assert peripheral["my_field"].write_interface == my_interface

    @pytest.mark.usefixtures("mock_driver")
    def test_read_subscript(self, mock_driver):
        peripheral = FpgaPeripheral(mock_driver, my_fields)
        field_address = my_fields["my_field"].address
        read_count = mock_driver.read_log.count(field_address)
        # read value
        peripheral["my_field"].value
        # number of reads of the address should increment
        assert mock_driver.read_log.count(field_address) > read_count

    @pytest.mark.usefixtures("mock_driver")
    def test_read_attr(self, mock_driver):
        peripheral = FpgaPeripheral(mock_driver, my_fields)
        field_address = my_fields["my_field"].address
        read_count = mock_driver.read_log.count(field_address)
        # read value
        peripheral.my_field.value
        # number of reads of the address should increment
        assert mock_driver.read_log.count(field_address) > read_count

    @pytest.mark.usefixtures("mock_driver")
    def test_write_subscript(self, mock_driver):
        peripheral = FpgaPeripheral(mock_driver, my_fields)
        field_address = my_fields["my_field"].address
        write_count = mock_driver.write_address_log.count(field_address)
        # write value
        assert 456 not in mock_driver.write_value_log
        peripheral["my_field"].value = 456
        # number of writes of the address should increment
        assert mock_driver.write_address_log.count(field_address) > write_count
        assert 456 in mock_driver.write_value_log

    @pytest.mark.usefixtures("mock_driver")
    def test_write_attr(self, mock_driver):
        peripheral = FpgaPeripheral(mock_driver, my_fields)
        field_address = my_fields["my_field"].address
        write_count = mock_driver.write_address_log.count(field_address)
        # write value
        assert 456 not in mock_driver.write_value_log
        peripheral.my_field.value = 456
        # number of writes of the address should increment
        assert mock_driver.write_address_log.count(field_address) > write_count
        assert 456 in mock_driver.write_value_log

    def test_attribute_discovery(self, mock_driver):
        """Check that properties are discovered and read/write detected"""

        class AttrTestClass(FpgaPeripheral):
            @property
            def test_ro(self) -> IclFpgaField:
                return IclFpgaField(user_write=False)

            @property
            def test_rw(self) -> IclFpgaField:
                return IclFpgaField(user_write=True)

            @test_rw.setter
            def test_rw(self, val):
                pass

        attr_test_class = AttrTestClass(mock_driver, my_fields)
        assert len(attr_test_class.user_attributes) == 2
        assert all(
            x in attr_test_class.user_attributes
            for x in ["test_ro", "test_rw"]
        )
        assert attr_test_class.test_ro.user_write is False
        assert attr_test_class.test_rw.user_write is True

    def test_attr_discover_properties(self, mock_driver):
        """Test discover properties flag"""

        class AttrTestClass(FpgaPeripheral):
            _user_attributes = {"test_ro", DISCOVER_PROPERTIES}

            @property
            def test_ro(self) -> IclFpgaField:
                return IclFpgaField(user_write=False)

            @property
            def test_rw(self) -> IclFpgaField:
                return IclFpgaField(user_write=True)

        attr_test_class = AttrTestClass(mock_driver, my_fields)
        assert "test_rw" in attr_test_class.user_attributes

    def test_attr_discover_registers(self, mock_driver):
        """Test register discovery flag"""

        class AttrTestClass(FpgaPeripheral):
            _user_attributes = {"test_ro", DISCOVER_REGISTERS}

            @property
            def test_ro(self) -> IclFpgaField:
                return IclFpgaField(user_write=False)

        attr_test_class = AttrTestClass(mock_driver, my_fields)
        assert "my_field" in attr_test_class.user_attributes

    def test_attr_discover_all(self, mock_driver):
        """Test discover all flag"""

        class AttrTestClass(FpgaPeripheral):
            _user_attributes = {"test_ro", DISCOVER_ALL}

            @property
            def test_ro(self) -> IclFpgaField:
                return IclFpgaField(user_write=False)

            @property
            def test_rw(self) -> IclFpgaField:
                return IclFpgaField(user_write=True)

        attr_test_class = AttrTestClass(mock_driver, my_fields)
        assert "test_rw" in attr_test_class.user_attributes
        assert "my_field" in attr_test_class.user_attributes

    def test_user_attributes(self, mock_driver):
        """Check that setting _user_attributes replaces discovery behaviour"""

        class AttrTestClass(FpgaPeripheral):
            _user_attributes = {"test_ro"}

            @property
            def test_ro(self) -> IclFpgaField:
                return IclFpgaField(user_write=False)

            @property
            def test_rw(self) -> IclFpgaField:
                return IclFpgaField(user_write=True)

            @test_rw.setter
            def test_rw(self, val):
                pass

        attr_test_class = AttrTestClass(mock_driver, my_fields)
        assert len(attr_test_class.user_attributes) == 1
        assert "test_ro" in attr_test_class.user_attributes

    def test_no_user_attributes(self, mock_driver):
        """Check that _user_attributes=None disables discovery behaviour"""

        class AttrTestClass(FpgaPeripheral):
            _user_attributes = None

            @property
            def test_ro(self) -> IclFpgaField:
                return IclFpgaField(user_write=False)

            @property
            def test_rw(self) -> IclFpgaField:
                return IclFpgaField(user_write=True)

            @test_rw.setter
            def test_rw(self, val):
                pass

        attr_test_class = AttrTestClass(mock_driver, my_fields)
        assert len(attr_test_class.user_attributes) == 0

    def test_field_config(self, mock_driver):
        """Check that setting values in _field_config overwrites the defaults,
        and that values that are not set retain the default values."""

        class FieldTestClass(FpgaPeripheral):
            _field_config = {
                "my_field": IclField(description="new desc.", type_=str)
            }

        field_test_class = FieldTestClass(mock_driver, my_fields)
        assert field_test_class.my_field.type_ == str
        assert (
            field_test_class.my_field.address == my_fields["my_field"].address
        )
        assert (
            field_test_class.my_field.description
            != my_fields["my_field"].description
        )
        assert field_test_class.my_field.description == "new desc."

    def test_no_attr_leakage(self, mock_driver):
        """Make sure user attributes are contained to their peripheral"""

        class AttrTest1(FpgaPeripheral):
            @property
            def test_1(self) -> IclFpgaField:
                return IclFpgaField()

        class AttrTest2(FpgaPeripheral):
            @property
            def test_2(self) -> IclFpgaField:
                return IclFpgaField()

        peripheral1 = AttrTest1(mock_driver, my_fields)
        peripheral2 = AttrTest2(mock_driver, my_fields)
        assert "test_2" not in peripheral1.user_attributes
        assert "test_1" not in peripheral2.user_attributes

    def test_no_config_leakage(self, mock_driver):
        """Make sure user attribute configuration settings don't leak out"""

        class AttrTestClass(FpgaPeripheral):
            _user_attributes = {"test_ro", DISCOVER_REGISTERS}

            @property
            def test_ro(self) -> IclFpgaField:
                return IclFpgaField(user_write=False)

        attr_test_class = AttrTestClass(mock_driver, my_fields)
        assert DISCOVER_REGISTERS not in attr_test_class.user_attributes


class TestFpgaPersonality:
    def test_class_map(self, mock_driver):
        """Check that peripheral_class is used to instantiate peripheral objects,
        defaulting to FpgaPeripheral if no class is specified"""
        _peripheral_class = {"test_peri_1": Mock}
        with patch(
            "ska_low_cbf_fpga.fpga_icl.FpgaPersonality._peripheral_class",
            _peripheral_class,
        ):
            personality = FpgaPersonality(mock_driver, DummyArgsMap())
            assert isinstance(personality.test_peri_1, Mock)
            assert isinstance(personality.test_peri_2, FpgaPeripheral)

    @pytest.mark.usefixtures("personality")
    def test_dir(self, personality):
        """Check that peripherals are listed in __dir__"""
        assert all(_ in personality.__dir__() for _ in DummyArgsMap().keys())

    def test_user_attr(self, mock_driver):
        test_attributes = {"foo", "bar"}

        class TestPersonality(FpgaPersonality):
            _user_attributes = test_attributes

            @property
            def foo(self) -> IclField[str]:
                return IclField(type_=str, value="FOO!")

            @property
            def bar(self) -> IclField[str]:
                return IclField(type_=str, value="BAR!")

        personality = TestPersonality(mock_driver, DummyArgsMap())
        assert all(x in personality.user_attributes for x in test_attributes)


@pytest.mark.usefixtures("simulated_fpga")
class TestIclFpgaBitfield:
    def test_type(self, simulated_fpga):
        assert isinstance(
            simulated_fpga.system.eth100g_fec_enable, IclFpgaField
        )
        assert isinstance(
            simulated_fpga.system.eth100g_fec_enable, IclFpgaBitfield
        )

    def test_value_type(self, simulated_fpga):
        assert isinstance(simulated_fpga.system.eth100g_fec_enable.value, bool)

    def test_read(self, simulated_fpga):
        simulated_fpga.system.eth100g_fec_whole_bitfield = 0
        assert not simulated_fpga.system.eth100g_fec_enable.value
        simulated_fpga.system.eth100g_fec_whole_bitfield = 0x10
        assert simulated_fpga.system.eth100g_fec_enable.value

    def test_write(self, simulated_fpga):
        # check that setting to True does not set other bits
        simulated_fpga.system.eth100g_fec_whole_bitfield = 0
        simulated_fpga.system.eth100g_fec_enable = True
        assert simulated_fpga.system.eth100g_fec_enable.value
        # this bit is at offset 4 per FPGA map
        assert simulated_fpga.system.eth100g_fec_whole_bitfield.value == 0x10

        # confirm that setting to False does not reset other bits
        simulated_fpga.system.eth100g_fec_whole_bitfield = 0xFFFF_FFFF
        simulated_fpga.system.eth100g_fec_enable = False
        assert not simulated_fpga.system.eth100g_fec_enable.value
        assert (
            simulated_fpga.system.eth100g_fec_whole_bitfield.value
            == 0xFFFF_FFEF
        )
