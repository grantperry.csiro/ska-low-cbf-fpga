# -*- coding: utf-8 -*-
#
# Copyright (c) 2021 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence Agreement
# See LICENSE.txt for more info.

from unittest.mock import Mock, call, patch

import numpy as np
import pytest


class DummyFlags:
    @property
    def normal(self):
        return 123

    @property
    def host_only(self):
        return 456


class MockBufferObject:
    flags = DummyFlags()

    def __init__(self, *args):
        self._size = args[1]

    def size(self):
        return self._size

    def read(self, size, offset):
        return np.zeros(size, dtype=np.uint8)


with patch.dict("sys.modules", pyxrt=Mock(bo=MockBufferObject)):
    from ska_low_cbf_fpga.args_xrt import ArgsXrt

from ska_low_cbf_fpga.args_fpga import (
    EXCHANGE_BUF_CONFIG,
    WORD_SIZE,
    ArgsFpgaDriver,
)


def mock_load_firmware(self):
    self._kernel = Mock()
    self._mem_group_ids = [1, 2, 3, 4, 5, 6, 7, 8]
    self._init_buffers()


@pytest.fixture
def mock_args_xrt():
    # skip the magic number check (tested for at the ArgsDriver level)
    with patch.object(ArgsFpgaDriver, "_check_magic", lambda x: True):
        # don't try to open a file that doesn't exist...
        with patch.object(ArgsXrt, "_load_firmware", mock_load_firmware):
            ArgsXrt._read_args_page = Mock()
            ArgsXrt._write_args_page = Mock()
            args_xrt = ArgsXrt(
                xcl_file="test_xcl_file_name",
                mem_config=None,
                card=0,
            )
            args_xrt._run = Mock()
            yield args_xrt


class TestArgsXrt:
    @pytest.mark.usefixtures("mock_args_xrt")
    def test_create(self, mock_args_xrt):
        mock_args_xrt

    @pytest.mark.usefixtures("mock_args_xrt")
    def test_small_read(self, mock_args_xrt):
        mock_args_xrt.read(4567, 1)
        # Note that for ArgsXrt._read_args_page both address and length are byte-based
        assert call(4567, WORD_SIZE) == mock_args_xrt._read_args_page.call_args

    @pytest.mark.usefixtures("mock_args_xrt")
    def test_read_by_pages(self, mock_args_xrt):
        """
        When a big read is requested, it should be broken down into page-sized chunks.
        """
        page_size = EXCHANGE_BUF_CONFIG.size  # bytes
        words_to_read = (page_size // WORD_SIZE) * 2 + 3
        mock_args_xrt.read(0, words_to_read)
        # Note that for ArgsXrt._read_args_page both address and length are byte-based
        assert (
            call(0, page_size) in mock_args_xrt._read_args_page.call_args_list
        )
        assert (
            call(page_size, page_size)
            in mock_args_xrt._read_args_page.call_args_list
        )
        assert (
            call(page_size * 2, 3 * WORD_SIZE)
            in mock_args_xrt._read_args_page.call_args_list
        )

    @pytest.mark.usefixtures("mock_args_xrt")
    def test_small_write(self, mock_args_xrt):
        mock_args_xrt.write(4567, 1)
        assert call(4567, 1) == mock_args_xrt._write_args_page.call_args

    @pytest.mark.usefixtures("mock_args_xrt")
    def test_write_by_pages(self, mock_args_xrt):
        """
        When a big write is requested, it should be broken down into page-sized chunks.
        Note that start addresses are byte-based, but lengths are word-based
        """
        page_size = EXCHANGE_BUF_CONFIG.size // WORD_SIZE
        test_size = page_size * 2 + 3
        # values = 1, 2, 3, ... test_size
        values = np.linspace(1, test_size, num=test_size, dtype=np.uint32)
        mock_args_xrt.write(0, values)

        # our read should be performed in 3 chunks
        # first full page
        first_call = mock_args_xrt._write_args_page.call_args_list[-3]
        assert 0 == first_call[0][0]
        np.testing.assert_array_equal(values[0:page_size], first_call[0][1])

        # second full page
        second_call = mock_args_xrt._write_args_page.call_args_list[-2]
        assert page_size * WORD_SIZE == second_call[0][0]
        np.testing.assert_array_equal(
            values[page_size : page_size * 2], second_call[0][1]
        )

        # final read, only 3 words
        last_call = mock_args_xrt._write_args_page.call_args_list[-1]
        assert page_size * 2 * WORD_SIZE == last_call[0][0]
        np.testing.assert_array_equal(values[-3:], last_call[0][1])
