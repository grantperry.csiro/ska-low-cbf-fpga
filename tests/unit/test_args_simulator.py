# -*- coding: utf-8 -*-
#
# Copyright (c) 2021 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence Agreement
# See LICENSE.txt for more info.

import numpy as np
import pytest

from ska_low_cbf_fpga.args_simulator import ArgsSimulator


class TestArgsSimulator:
    def test_create(self, simulator_map_path):
        ArgsSimulator.create_from_file(simulator_map_path)

    @pytest.mark.usefixtures("simulator")
    def test_build(self, simulator):
        assert simulator.get_map_build() == 0x12345678

    @pytest.mark.usefixtures("simulator")
    def test_write_array(self, simulator):
        values = np.array([1, 2, 3, 4], dtype=np.uint32)
        simulator.write(8, values)
        np.testing.assert_array_equal(simulator.read(8, values.size), values)
