# -*- coding: utf-8 -*-
#
# Copyright (c) 2021 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence Agreement
# See LICENSE.txt for more info.
# import pytest
# from unittest.mock import Mock
#
# import numpy as np
# import pyopencl as cl
#
# from ska_low_cbf_fpga.alveo_cl import (
#     AlveoCL,
#     MemConfig,
#     WORD_SIZE,
#     WORD_TYPE,
# )
#
#
# def get_rand_to_match_buf(fpga: AlveoCL, index: int):
#     """Return an array of random numbers sized to match an AlveoCL shared buffer"""
#     print(f"Creating array of random numbers for buffer {index}")
#     return (np.random.rand(fpga.buf_np[index].size) * (2 ** (8 * WORD_SIZE))).astype(
#         WORD_TYPE
#     )
#
#
# def write_to_buf(fpga: AlveoCL, index: int, array):
#     fpga.buf_np[index].put(np.arange(fpga.buf_np[index].size), array)
#     print(f"Transferring to FPGA {fpga.buf_np[index]}")
#     with fpga._lock:
#         cl.enqueue_migrate_mem_objects(fpga._queue, [fpga.shared_buf[index]])
#         fpga._queue.finish()
#     # replace local buffer with zeros so we can check read-back
#     fpga.buf_np[index].fill(0)
#     assert all(fpga.buf_np[index] == 0)
#
#
# @pytest.fixture(scope="class")
# def cl_fpga():
#     print("\n*** Setting Up CL FPGA Fixture ***\n")
#     logging = Mock()
#     xclbin = "tests/firmware/lv.xclbin"
#     mem = [
#         MemConfig(1024 * 4, True),
#         MemConfig(1 << 30, True),
#         MemConfig(128 << 20, True),
#     ]
#     card = 9
#     with AlveoCL(xclbin, logging, mem, card, poll_time=1) as fpga:
#         yield fpga
#         print("\n*** Tearing Down CL FPGA Fixture ***\n")
#     print("*** Finished teardown ***")
#
#
# @pytest.mark.fpga
# class TestHbm:
#     @pytest.mark.usefixtures("cl_fpga")
#     def test_hbm_read(self, cl_fpga):
#         index = 1
#         # write random numbers
#         random_numbers = get_rand_to_match_buf(cl_fpga, index)
#         write_to_buf(cl_fpga, index, random_numbers)
#         # read back
#         read_back = cl_fpga.read_buf(index)
#         # check same
#         assert all(random_numbers == read_back)
#
#     @pytest.mark.usefixtures("cl_fpga")
#     def test_hbm_save(self, cl_fpga):
#         index = 1
#         # write random numbers
#         random_numbers = get_rand_to_match_buf(cl_fpga, index)
#         write_to_buf(cl_fpga, index, random_numbers)
#         # save to disk
#         cl_fpga.save_buf(1, "testbuf")
#         # check file contents are correct
#         assert all(np.load("testbuf.npy") == random_numbers)
