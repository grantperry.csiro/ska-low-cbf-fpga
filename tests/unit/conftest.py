# -*- coding: utf-8 -*-
#
# Copyright (c) 2021 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence Agreement
# See LICENSE.txt for more info.
import os
from unittest.mock import patch

import pytest

from ska_low_cbf_fpga import ArgsSimulator
from ska_low_cbf_fpga.args_fpga import ARGS_MAGIC, ArgsFpgaDriver
from ska_low_cbf_fpga.args_map import ArgsFieldInfo, ArgsMap
from ska_low_cbf_fpga.fpga_icl import (
    FpgaPeripheral,
    FpgaPersonality,
    IclFpgaField,
)


class MockDriver(ArgsFpgaDriver):
    def _setup(self, read_value=ARGS_MAGIC):
        self.read_log = []
        self.write_address_log = []
        self.write_value_log = []
        self._read_value = read_value
        self.load_firmware_called = False
        self.init_called = True

    def _load_firmware(self):
        self.load_firmware_called = True

    def read(self, source, length=None):
        if isinstance(source, IclFpgaField):
            length = source.length
            source = source.address
        self.read_log.append(source)
        if length is None:
            length = 1
        if length == 1:
            return self._read_value
        else:
            return [self._read_value] * length

    def write(self, destination, value):
        if isinstance(destination, IclFpgaField) or (
            hasattr(destination, "address") and hasattr(destination, "length")
        ):
            length = destination.length
            destination = destination.address
        self.write_address_log.append(destination)
        self.write_value_log.append(value)


@pytest.fixture()
def mock_driver():
    mock_driver = MockDriver()
    mock_driver._read_value = 0
    yield mock_driver


my_fields = {"my_field": ArgsFieldInfo(description="for testing", address=123)}
"""minimal address map to use in an FpgaPeripheral"""


@pytest.fixture(scope="function")
def mock_peripheral(mock_driver):
    peripheral = FpgaPeripheral(mock_driver, my_fields)
    return peripheral


class DummyArgsMap(ArgsMap):
    def __init__(self, build=None, map_dir=None):
        self._locked = False
        self["test_peri_1"] = {}
        peri_1_register_1 = ArgsFieldInfo(
            description="test peri 1, register 1", address=123, length=1
        )
        self["test_peri_1"]["reg1"] = peri_1_register_1
        self["test_peri_2"] = {}
        self["test_peri_2"]["reg1"] = ArgsFieldInfo(
            description="test peri 2, register 1", address=123, length=1
        )
        self._locked = True


@pytest.fixture(scope="function")
def personality(mock_driver):
    yield FpgaPersonality(interfaces=mock_driver, map_=DummyArgsMap())


@pytest.fixture
def sample_map():
    # Use the dictionary defined in the raw_map function, rather than loading a file.
    with patch("ska_low_cbf_fpga.args_map.load_map") as load_map:
        load_map.return_value = raw_map()
        sample_map = ArgsMap.create_from_file(0, "path_ignored_due_to_patch")
    yield sample_map


@pytest.fixture(scope="session")
def simulator_map_path():
    return os.path.join(os.path.dirname(__file__), "fpgamap_12345678.py")


@pytest.fixture
def simulator(simulator_map_path):
    yield ArgsSimulator.create_from_file(simulator_map_path)


@pytest.fixture
def simulated_fpga(simulator_map_path):
    simulator = ArgsSimulator.create_from_file(simulator_map_path)
    args_map = ArgsMap.create_from_file(
        simulator.get_map_build(), os.path.dirname(simulator_map_path)
    )
    yield FpgaPersonality(simulator, args_map)


def raw_map():
    return {
        "system": {
            "slaves": {
                "system": {
                    "fields": {
                        "args_magic_number": {
                            "access_mode": "SP",
                            "bit_offset": 0,
                            "default": 4184502273,
                            "description": "args magic number for address 0",
                            "start": 0,
                            "step": 1,
                            "stop": 1,
                            "width": 32,
                        },
                        "args_map_build": {
                            "access_mode": "SP",
                            "bit_offset": 0,
                            "default": 554172945,
                            "description": "args map build date & hour",
                            "start": 1,
                            "step": 1,
                            "stop": 2,
                            "width": 32,
                        },
                        "build_date": {
                            "access_mode": "RO",
                            "bit_offset": 0,
                            "default": 0,
                            "description": "Build Date of firmware, Day/Month/Year, readable in HEX, hand coded and not tied to autogeneration currently",
                            "start": 4,
                            "step": 1,
                            "stop": 5,
                            "width": 32,
                        },
                        "eth100g_fec_enable": {
                            "access_mode": "RW",
                            "bit_offset": 0,
                            "default": 0,
                            "description": "Set to 1 to enable FEC, 0 to disable",
                            "start": 16,
                            "step": 1,
                            "stop": 17,
                            "width": 1,
                        },
                        "eth100g_locked": {
                            "access_mode": "RO",
                            "bit_offset": 0,
                            "default": 0,
                            "description": "100G ethernet is up",
                            "start": 11,
                            "step": 1,
                            "stop": 12,
                            "width": 1,
                        },
                        "eth100g_rx_bad_code": {
                            "access_mode": "RO",
                            "bit_offset": 0,
                            "default": 0,
                            "description": "Total packets received on the 100G ethernet with 64B/66B code violations",
                            "start": 14,
                            "step": 1,
                            "stop": 15,
                            "width": 32,
                        },
                        "eth100g_rx_bad_fcs": {
                            "access_mode": "RO",
                            "bit_offset": 0,
                            "default": 0,
                            "description": "Total packets received on the 100G ethernet with bad FCS since it was reset",
                            "start": 13,
                            "step": 1,
                            "stop": 14,
                            "width": 32,
                        },
                        "eth100g_rx_total_packets": {
                            "access_mode": "RO",
                            "bit_offset": 0,
                            "default": 0,
                            "description": "Total packets received on the 100G ethernet since it was reset",
                            "start": 12,
                            "step": 1,
                            "stop": 13,
                            "width": 32,
                        },
                        "eth100g_tx_total_packets": {
                            "access_mode": "RO",
                            "bit_offset": 0,
                            "default": 0,
                            "description": "Total packets sent on the 100G ethernet since it was reset",
                            "start": 15,
                            "step": 1,
                            "stop": 16,
                            "width": 32,
                        },
                        "firmware_label": {
                            "access_mode": "RO",
                            "bit_offset": 0,
                            "default": 0,
                            "description": "Firmware Label",
                            "start": 8,
                            "step": 1,
                            "stop": 9,
                            "width": 32,
                        },
                        "firmware_major_version": {
                            "access_mode": "RO",
                            "bit_offset": 0,
                            "default": 0,
                            "description": "Major version number",
                            "start": 5,
                            "step": 1,
                            "stop": 6,
                            "width": 16,
                        },
                        "firmware_minor_version": {
                            "access_mode": "RO",
                            "bit_offset": 0,
                            "default": 0,
                            "description": "Minor version number",
                            "start": 6,
                            "step": 1,
                            "stop": 7,
                            "width": 16,
                        },
                        "firmware_patch_version": {
                            "access_mode": "RO",
                            "bit_offset": 0,
                            "default": 0,
                            "description": "Patch version number",
                            "start": 7,
                            "step": 1,
                            "stop": 8,
                            "width": 16,
                        },
                        "firmware_personality": {
                            "access_mode": "RO",
                            "bit_offset": 0,
                            "default": 0,
                            "description": "Firmware function - PST/PSS/ZOOM/CORR",
                            "start": 9,
                            "step": 1,
                            "stop": 10,
                            "width": 32,
                        },
                        "qsfpgty_resets": {
                            "access_mode": "RW",
                            "bit_offset": 0,
                            "default": 0,
                            "description": "Reset 100G ethernet interface",
                            "start": 3,
                            "step": 1,
                            "stop": 4,
                            "width": 1,
                        },
                        "status_clocks_locked": {
                            "access_mode": "RO",
                            "bit_offset": 0,
                            "default": 0,
                            "description": "All clocks locked",
                            "start": 2,
                            "step": 1,
                            "stop": 3,
                            "width": 1,
                        },
                        "time_uptime": {
                            "access_mode": "RO",
                            "bit_offset": 0,
                            "default": 0,
                            "description": "Number of seconds since programming",
                            "start": 10,
                            "step": 1,
                            "stop": 11,
                            "width": 32,
                        },
                    },
                    "start": 0,
                    "step": 17,
                    "stop": 17,
                    "type": "REG",
                }
            },
            "start": 100,  # Note: this is 0 in a real map!
            "step": 17,
            "stop": 117,
        },
    }
