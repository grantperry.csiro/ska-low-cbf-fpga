# -*- coding: utf-8 -*-
#
# Copyright (c) 2021 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence Agreement
# See LICENSE.txt for more info.

import numpy as np
import pytest

from ska_low_cbf_fpga.register_load import load, register_grammar

BUILD_DATE = """[system.system.build_date][0]
0xFEDCBA98
"""

ARRAY = """[array.my_array.data][8]
0x00000000
0x00000001
0x00000002
0x00000003
0x00000004
0x00000005
0x00000006
0x00000007
0x00000008
0x00000009
0x0000000A
0x0000000B
0x0000000C
0x0000000D
0x0000000E
0x0000000F
"""


class TestRegisterLoader:
    @pytest.mark.parametrize("text", [BUILD_DATE, ARRAY])
    def test_grammar(self, text):
        register_grammar.parse(text)

    @pytest.mark.usefixtures("simulated_fpga")
    def test_load_single(self, simulated_fpga):
        assert simulated_fpga["system"]["build_date"] == 0
        load(simulated_fpga, BUILD_DATE)
        assert simulated_fpga["system"]["build_date"] == 0xFEDCBA98

    @pytest.mark.usefixtures("simulated_fpga")
    def test_load_array(self, simulated_fpga):
        load(simulated_fpga, ARRAY)
        expected = [_ for _ in range(16)]
        # test data is offset by 8 bytes, which is 2 words
        actual = simulated_fpga.array.data.value[2:18]
        np.testing.assert_array_equal(actual, expected)

    @pytest.mark.usefixtures("simulated_fpga")
    def test_load_multiple(self, simulated_fpga):
        assert simulated_fpga["system"]["build_date"] == 0
        load(simulated_fpga, ARRAY + BUILD_DATE)
        assert simulated_fpga["system"]["build_date"] == 0xFEDCBA98
        expected_array = [_ for _ in range(16)]
        actual_array = simulated_fpga.array.data.value[2:18]
        np.testing.assert_array_equal(actual_array, expected_array)
