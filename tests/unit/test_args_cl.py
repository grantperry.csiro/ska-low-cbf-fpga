# -*- coding: utf-8 -*-
#
# Copyright (c) 2021 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence Agreement
# See LICENSE.txt for more info.

from unittest.mock import Mock, call, patch

import numpy as np
import pytest

from ska_low_cbf_fpga.args_cl import ArgsCl
from ska_low_cbf_fpga.args_fpga import (
    EXCHANGE_BUF_CONFIG,
    WORD_SIZE,
    ArgsFpgaDriver,
)


def mock_load_firmware(self):
    self._init_cl()
    self._kernel = Mock()
    self._init_buffers()


@pytest.fixture
def mock_args_cl():
    # skip the magic number check (tested for at the ArgsDriver level)
    with patch.object(ArgsFpgaDriver, "_check_magic", lambda x: True):
        with patch("ska_low_cbf_fpga.args_cl.cl") as mock_cl:
            # don't try to open a file that doesn't exist...
            with patch.object(ArgsCl, "_load_firmware", mock_load_firmware):
                ArgsCl._read_args_page = Mock()
                ArgsCl._write_args_page = Mock()
                mock_platform = Mock()
                mock_platform.vendor = "Xilinx"
                mock_cl.get_platforms.return_value = [mock_platform]
                mock_device = Mock()
                mock_platform.get_devices.return_value = [mock_device]
                args_cl = ArgsCl(
                    xcl_file="test_xcl_file_name",
                    mem_config=None,
                    card=0,
                )
                yield args_cl


class TestArgsCl:
    def test_no_platform(self):
        with pytest.raises(Exception) as no_platform:
            with patch("ska_low_cbf_fpga.args_cl.cl"):
                ArgsCl(
                    xcl_file="xcl_file",
                    mem_config=None,
                    card=100,
                )
        assert "Xilinx CL platform" in str(no_platform.value)

    def test_no_xilinx_platform(self):
        with pytest.raises(Exception) as no_xilinx:
            mock_platform = Mock()
            mock_platform.vendor = "not-xilinx"
            with patch("ska_low_cbf_fpga.args_cl.cl") as mock_cl:
                mock_cl.get_platforms.return_value = [mock_platform]
                ArgsCl(
                    xcl_file="xcl_file",
                    mem_config=None,
                    card=100,
                )
        assert "Xilinx CL platform" in str(no_xilinx.value)

    def test_bad_card_index(self):
        with pytest.raises(IndexError):
            with patch("ska_low_cbf_fpga.args_cl.cl") as mock_cl:
                mock_platform = Mock()
                mock_platform.vendor = "Xilinx"
                mock_cl.get_platforms.return_value = [mock_platform]
                mock_device = Mock()
                mock_platform.get_devices.return_value = [mock_device]
                ArgsCl(
                    xcl_file="xcl_file",
                    mem_config=None,
                    card=100,
                )

    def test_bad_xcl_file_path(self):
        with pytest.raises(FileNotFoundError) as file_not_found:
            with patch("ska_low_cbf_fpga.args_cl.cl") as mock_cl:
                mock_platform = Mock()
                mock_platform.vendor = "Xilinx"
                mock_cl.get_platforms.return_value = [mock_platform]
                mock_device = Mock()
                mock_platform.get_devices.return_value = [mock_device]
                ArgsCl(
                    xcl_file="test_xcl_file_name",
                    mem_config=None,
                    card=0,
                )
        assert "test_xcl_file_name" in str(file_not_found.value)

    @pytest.mark.usefixtures("mock_args_cl")
    def test_create_cl(self, mock_args_cl):
        mock_args_cl

    @pytest.mark.usefixtures("mock_args_cl")
    def test_small_read(self, mock_args_cl):
        mock_args_cl.read(4567, 1)
        assert call(4567, 1) == mock_args_cl._read_args_page.call_args

    @pytest.mark.usefixtures("mock_args_cl")
    def test_read_by_pages(self, mock_args_cl):
        """
        When a big read is requested, it should be broken down into page-sized chunks.
        Note that start addresses are byte-based, but lengths are word-based
        """
        page_size = EXCHANGE_BUF_CONFIG.size // WORD_SIZE
        mock_args_cl.read(0, page_size * 2 + 3)
        assert (
            call(0, page_size) in mock_args_cl._read_args_page.call_args_list
        )
        assert (
            call(page_size * WORD_SIZE, page_size)
            in mock_args_cl._read_args_page.call_args_list
        )
        assert (
            call(page_size * 2 * WORD_SIZE, 3)
            in mock_args_cl._read_args_page.call_args_list
        )

    @pytest.mark.usefixtures("mock_args_cl")
    def test_small_write(self, mock_args_cl):
        mock_args_cl.write(4567, 1)
        assert call(4567, 1) == mock_args_cl._write_args_page.call_args

    @pytest.mark.usefixtures("mock_args_cl")
    def test_write_by_pages(self, mock_args_cl):
        """
        When a big write is requested, it should be broken down into page-sized chunks.
        Note that start addresses are byte-based, but lengths are word-based
        """
        page_size = EXCHANGE_BUF_CONFIG.size // WORD_SIZE
        test_size = page_size * 2 + 3
        # values = 1, 2, 3, ... test_size
        values = np.linspace(1, test_size, num=test_size, dtype=np.uint32)
        mock_args_cl.write(0, values)

        # our read should be performed in 3 chunks
        # first full page
        first_call = mock_args_cl._write_args_page.call_args_list[-3]
        assert 0 == first_call[0][0]
        np.testing.assert_array_equal(values[0:page_size], first_call[0][1])

        # second full page
        second_call = mock_args_cl._write_args_page.call_args_list[-2]
        assert page_size * WORD_SIZE == second_call[0][0]
        np.testing.assert_array_equal(
            values[page_size : page_size * 2], second_call[0][1]
        )

        # final read, only 3 words
        last_call = mock_args_cl._write_args_page.call_args_list[-1]
        assert page_size * 2 * WORD_SIZE == last_call[0][0]
        np.testing.assert_array_equal(values[-3:], last_call[0][1])
