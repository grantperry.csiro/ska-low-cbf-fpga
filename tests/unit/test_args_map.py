# -*- coding: utf-8 -*-
#
# Copyright (c) 2021 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence Agreement
# See LICENSE.txt for more info.
import os

import pytest

from ska_low_cbf_fpga.args_fpga import WORD_SIZE
from ska_low_cbf_fpga.args_map import ArgsMap


class TestMap:
    def test_load(self):
        """Load a map file from disk (fpgamap_12345678.py in our directory)"""
        ArgsMap.create_from_file(0x12345678, os.path.dirname(__file__))

    def test_load_wrong_path(self):
        """Using the wrong path should fail"""
        with pytest.raises(FileNotFoundError):
            ArgsMap.create_from_file(
                0x12345678, os.path.join(os.path.dirname(__file__), os.pardir)
            )

    def test_load_wrong_build(self):
        """Using the wrong build number should fail"""
        with pytest.raises(FileNotFoundError):
            ArgsMap.create_from_file(12345678, os.path.dirname(__file__))

    @pytest.mark.usefixtures("sample_map")
    def test_peripherals(self, sample_map):
        assert set(sample_map.keys()) == {"system"}

    @pytest.mark.usefixtures("sample_map")
    def test_field_names(self, sample_map):
        assert set(sample_map["system"].keys()) == {
            "args_magic_number",
            "args_map_build",
            "build_date",
            "eth100g_fec_enable",
            "eth100g_locked",
            "eth100g_rx_bad_code",
            "eth100g_rx_bad_fcs",
            "eth100g_rx_total_packets",
            "eth100g_tx_total_packets",
            "firmware_label",
            "firmware_major_version",
            "firmware_minor_version",
            "firmware_patch_version",
            "firmware_personality",
            "qsfpgty_resets",
            "status_clocks_locked",
            "time_uptime",
        }

    @pytest.mark.parametrize(
        "field, address",
        [
            ("args_magic_number", 100 * WORD_SIZE),
            ("args_map_build", 101 * WORD_SIZE),
            ("build_date", 104 * WORD_SIZE),
            ("firmware_label", 108 * WORD_SIZE),
            ("firmware_major_version", 105 * WORD_SIZE),
            ("firmware_minor_version", 106 * WORD_SIZE),
            ("firmware_patch_version", 107 * WORD_SIZE),
            ("firmware_personality", 109 * WORD_SIZE),
            ("time_uptime", 110 * WORD_SIZE),
        ],
    )
    @pytest.mark.usefixtures("sample_map")
    def test_field_address(self, sample_map, field, address):
        assert sample_map["system"][field].address == address

    @pytest.mark.usefixtures("sample_map")
    def test_lock(self, sample_map):
        """After creation, we shouldn't be able to alter peripherals"""
        with pytest.raises(Exception):
            sample_map["system"] = None
